FROM node:carbon

WORKDIR /tmp
ADD package.json /tmp/package.json
#ADD wait-for-it.sh ./wait-for-it.sh
RUN npm install
RUN mkdir -p /usr/src/app
RUN cp -a /tmp/node_modules /usr/src/app

WORKDIR /usr/src/app

COPY package*.json ./

ADD . /usr/src/app

EXPOSE 3050:3050

CMD npm run start