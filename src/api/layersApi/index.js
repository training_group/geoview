import axios from "axios";
import Layer from "../../core/layers/model/layer";
import BaseLayer from "../../core/layers/model/baseLayer";
import layerGeometryTypes from "../../core/layers/model/enum/layerGeometryTypes";
import layerFieldTypes from "../../core/layers/model/enum/layerFieldTypes";

import LayerField from "../../core/layers/model/layerField";

const baseLayersSrc = require("./baseLayers.json");

class LayersApi {
  constructor({ apiConfig, src }) {
    this.apiConfig = apiConfig;
    this.spatialReference = {
      latestWkid: src || 3857
    };
  }

  static extractData(serverResponse) {
    if (serverResponse.status === 200) {
      return serverResponse.data;
    }

    throw new Error("Ошибка запроса к серверу: ");
  }

  static getLayerGeometryType(geometryType) {
    const mapper = {
      Point: layerGeometryTypes.point,
      LineString: layerGeometryTypes.line,
      Polygon: layerGeometryTypes.polygon
    };
    return mapper[geometryType];
  }

  static getLayerFieldType(fieldType) {
    const mapper = {
      number: layerFieldTypes.number,
      integer: layerFieldTypes.number,
      string: layerFieldTypes.string,
      date: layerFieldTypes.date,
      double: layerFieldTypes.number
    };

    return mapper[fieldType];
  }

  static getLayerFields(layerFields) {
    return layerFields.map(layerField => {
      const { name, alias, editable, nullable, type } = layerField;

      let visible = true;
      const layerFieldType = LayersApi.getLayerFieldType(type);
      if (!layerFieldType) {
        visible = false;
      }

      return new LayerField({
        name,
        alias,
        type: layerFieldType,
        visible,
        isEditable: editable,
        required: !nullable,
        inputType: type
      });
    });
  }

  static async getLayerFeatures(layer) {
    const { id, rootURL, spatialReference } = layer;
    const serverResponse = await axios.post(`${rootURL}read/features`, {
      id,
      spatialReference
    });
    return LayersApi.extractData(serverResponse);
  }

  static async getLayerData(layer) {
    const { id, spatialReference, rootURL } = layer;
    const serverResponse = await axios.post(`${rootURL}read/layer`, {
      id,
      spatialReference
    });
    return LayersApi.extractData(serverResponse);
  }

  static getLayersData(layers) {
    const results = Promise.all(
      Object.keys(layers).map(async layerId => {
        const layer = layers[layerId];
        const { data: layerMetaData } = await LayersApi.getLayerData(layer);
        const { data: geojson } = await LayersApi.getLayerFeatures(layer);
        const data = { id: layerId, layerMetaData, data: geojson };
        return data;
      })
    );

    return results;
  }

  async getLayers(user) {
    const path = `${this.apiConfig}read/layers`;

    const serverResponse = await axios.post(path, { userId: user });
    const { data: layersName } = LayersApi.extractData(serverResponse);

    const layersData = {};

    layersName.forEach(layerName => {
      const layerData = {
        id: layerName,
        title: layerName,
        visible: true,
        editable: true,
        fields: [],
        rootURL: `${this.apiConfig}`,
        spatialReference: this.spatialReference
      };
      layersData[layerName] = layerData;
    });

    const layers = await LayersApi.getLayersData(layersData);

    const userLayersArray = layers.reduce((accum, layerItem) => {
      const { id, layerMetaData, data } = layerItem;
      let layerData = layersData[id];
      const { geometryType } = layerMetaData;
      layerData.geometryType = LayersApi.getLayerGeometryType(geometryType);
      const layerFields = LayersApi.getLayerFields(layerMetaData.fields);
      layerData.fields = layerFields;
      layerData = {
        ...layerData,
        objectIdField: layerMetaData.objectIdField,
        displayField: layerMetaData.displayField,
        data
      };

      return { ...accum, [id]: Layer.create(layerData) };
    }, {});

    const baseLayers = baseLayersSrc.map(item => {
      const { id, previewUrl } = item;
      return new BaseLayer({
        id,
        title: id,
        previewUrl,
        data: item
      });
    });

    return { baseLayers, layers: userLayersArray };
  }

  async updateLayer({ id, features }) {
    const path = `${this.apiConfig}update/features`;
    const { spatialReference } = this;

    const serverResponse = await axios.post(path, {
      id,
      features,
      spatialReference
    });
    const { data: count } = LayersApi.extractData(serverResponse);

    const layer = {
      id,
      spatialReference,
      rootURL: this.apiConfig
    };
    const { data: layerMetaData } = await LayersApi.getLayerData(layer);
    const { data } = await LayersApi.getLayerFeatures(layer);

    const layerId = id;
    let layerData = {
      id: layerId,
      title: layerId,
      visible: true,
      editable: true,
      fields: [],
      rootURL: `${this.apiConfig}`,
      spatialReference: this.spatialReference
    };
    const { geometryType } = layerMetaData;
    layerData.geometryType = LayersApi.getLayerGeometryType(geometryType);
    const layerFields = LayersApi.getLayerFields(layerMetaData.fields);
    layerData.fields = layerFields;
    layerData = {
      ...layerData,
      objectIdField: layerMetaData.objectIdField,
      displayField: layerMetaData.displayField,
      data
    };

    return Layer.create(layerData);
  }

  async removeFeatures({ id, features }) {
    const path = `${this.apiConfig}delete/features`;
    const { spatialReference } = this;

    const serverResponse = await axios.post(path, {
      id,
      features
    });
    const { data: count } = LayersApi.extractData(serverResponse);

    const layer = {
      id,
      spatialReference,
      rootURL: this.apiConfig
    };
    const { data: layerMetaData } = await LayersApi.getLayerData(layer);
    const { data } = await LayersApi.getLayerFeatures(layer);

    const layerId = id;
    let layerData = {
      id: layerId,
      title: layerId,
      visible: true,
      editable: true,
      fields: [],
      rootURL: `${this.apiConfig}`,
      spatialReference: this.spatialReference
    };
    const { geometryType } = layerMetaData;
    layerData.geometryType = LayersApi.getLayerGeometryType(geometryType);
    const layerFields = LayersApi.getLayerFields(layerMetaData.fields);
    layerData.fields = layerFields;
    layerData = {
      ...layerData,
      objectIdField: layerMetaData.objectIdField,
      displayField: layerMetaData.displayField,
      data
    };

    return Layer.create(layerData);
  }
}

export default LayersApi;
