import Map from "ol/Map";
import View from "ol/View";
import GeoJSON from "ol/format/GeoJSON";
import { Tile as TileLayer, Vector as VectorLayer } from "ol/layer";
import { OSM, Vector as VectorSource } from "ol/source";
import Select from "ol/interaction/Select";

export default class OpenLayersApi {
  static async init(id) {
    const basemap = new TileLayer({
      source: new OSM()
    });

    //center: [3370305.2940428695, 8385586.493826952],
    const view = new View({
      projection: "EPSG:3857",
      center: [3370305.2940428695, 8325586.493826952],
      zoom: 11
    });

    const mapApi = new Map({
      layers: [basemap],
      target: id,
      view
    });

    return new OpenLayersApi(mapApi, view);
  }

  constructor(api, view) {
    this.api = api;
    this.view = view;
    this.baseMaps = {};
    this.eventHandlers = [];
    this.storeLayers = {};
  }

  addBaseLayers(baseLayers) {}

  setCurrentBaseMap(basemapId) {}

  addLayer(layer) {
    const { data, visible, id } = layer;
    if (visible) {
      const vectorSource = new VectorSource({
        features: new GeoJSON().readFeatures(data)
      });

      const vectorLayer = new VectorLayer({
        source: vectorSource
      });
      this.storeLayers[id] = vectorLayer;
      this.api.addLayer(vectorLayer);
    }
  }

  updateLayers(layers) {
    Object.keys(layers).map(layerId => {
      const layer = layers[layerId];
      const { visible, id } = layer;
      if (!visible) {
        const vectorLayer = this.storeLayers[id];
        this.api.removeLayer(vectorLayer);
      } else {
        this.addLayer(layer);
      }
    });
  }

  zoomTo(type, geometryData) {}

  static featuresToGeoJson(features) {
    return new GeoJSON().writeFeaturesObject(features);
  }

  setIdentification(identification) {
    const selectSingleClick = new Select();
    this.api.addInteraction(selectSingleClick);
    selectSingleClick.on("select", function(data) {
      identification(
        OpenLayersApi.featuresToGeoJson(data.selected).features || []
      );
    });
  }

  cancelTool() {}
}
