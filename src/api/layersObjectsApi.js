import axios from "axios";

import spatialRelations from "../core/spatialRelations/enum/spatialRelations";
import LayersObjectsRepository from "../core/layersObjects/repository/layersObjectsRepository";
import LayersObject from "../core/layersObjects/model/layersObject";
import LayersObjectGeometry from "../core/layersObjects/model/layersObjectGeometry";
import layerGeometryTypes from "../core/layers/model/enum/layerGeometryTypes";
import LayersObjectAttribute from "../core/layersObjects/model/layersObjectAttribute";
import LayerField from "../core/layers/model/layerField";
import layerFieldTypes from "../core/layers/model/enum/layerFieldTypes";

const spatialRelsESRI = {
  [spatialRelations.intersects.id]: "esriSpatialRelIntersects"
};

const geometryTypesEsri = {
  [layerGeometryTypes.point]: "esriGeometryPoint",
  [layerGeometryTypes.multiPoint]: "esriGeometryMultiPoint",
  [layerGeometryTypes.line]: "esriGeometryPolyline",
  [layerGeometryTypes.polygon]: "esriGeometryPolygon"
};

class LayersObjectsApi extends LayersObjectsRepository {
  static extractData(serverResponse) {
    if (serverResponse.status === 200) {
      return serverResponse.data;
    }
    throw new Error("Ошибка запроса к серверу: ");
  }

  static executeAnyway(layer, request) {
    return new Promise(resolve => {
      request
        .then(result => {
          const data = LayersObjectsApi.extractData(result);
          resolve({ layer, data });
        })
        .catch(e => {
          resolve(e);
        });
    });
  }

  static createLayerObjectGeometryAttribute(name, alias, value) {
    const coordinateField = new LayerField({
      name,
      alias,
      type: layerFieldTypes.number,
      visible: true,
      isEditable: true,
      required: true
    });

    const attribute = new LayersObjectAttribute(coordinateField, value);
    return attribute;
  }

  static getCoordinateAttributes(geometryType, esriGeometry) {
    const coordinates = [];
    const mapping = {
      [layerGeometryTypes.polygon]: "rings",
      [layerGeometryTypes.multiPoint]: "points",
      [layerGeometryTypes.line]: "paths"
    };

    if (layerGeometryTypes.point === geometryType) {
      const x = LayersObjectsApi.createLayerObjectGeometryAttribute(
        "x",
        "x",
        esriGeometry.x
      );
      const y = LayersObjectsApi.createLayerObjectGeometryAttribute(
        "y",
        "y",
        esriGeometry.y
      );
      coordinates.push([x, y]);
    } else {
      const field = mapping[geometryType];
      const coordinatesArray = esriGeometry[field];
      coordinatesArray[0].forEach((point, index) => {
        const number = index + 1;
        const nameX = `x${number}`;
        const nameY = `y${number}`;
        const xValue = point[0];
        const yValue = point[1];
        const x = LayersObjectsApi.createLayerObjectGeometryAttribute(
          nameX,
          "x",
          xValue
        );
        const y = LayersObjectsApi.createLayerObjectGeometryAttribute(
          nameY,
          "y",
          yValue
        );
        coordinates.push([x, y]);
      });
    }

    return coordinates;
  }

  static getEsriRequestGeometry(coordinates) {
    let esriGeometry = "";
    if (!coordinates || coordinates.length === 0) {
      return esriGeometry;
    }

    coordinates.forEach(coordinate => {
      esriGeometry += coordinate.join();
    });

    return esriGeometry;
  }

  constructor() {
    super();
    this.request = axios;
  }

  makeRequestOnLayer(url, requestParams) {
    const formData = new URLSearchParams();

    Object.keys(requestParams).forEach(paramKey => {
      formData.append(paramKey, requestParams[paramKey]);
    });

    return this.request.post(url, formData);
  }

  async findBy(layersObjectsSpecification) {
    const {
      layers,
      geometry,
      spatialRel,
      distance
    } = layersObjectsSpecification;

    if (!layers || layers.length === 0) {
      return [];
    }

    const requestParams = {
      distance,
      units: "esriSRUnit_Meter",
      outFields: "*",
      f: "json"
    };

    if (geometry) {
      const { coordinates, layerGeometryType } = geometry;
      const esriGeometry = LayersObjectsApi.getEsriRequestGeometry(coordinates);
      requestParams.geometry = esriGeometry;
      requestParams.geometryType = geometryTypesEsri[layerGeometryType];
      requestParams.spatialRel = spatialRelsESRI[spatialRel.id];
    }

    const identifiedLayersObjects = await Promise.all(
      layers.map(layer => {
        const {
          data: { url }
        } = layer;

        const fullUrl = `${url}/query`;

        const runningRequest = this.makeRequestOnLayer(fullUrl, requestParams);

        return LayersObjectsApi.executeAnyway(layer, runningRequest);
      })
    );

    const layersObjects = [];

    identifiedLayersObjects.forEach(result => {
      const { layer, data } = result;

      const {
        id,
        data: { displayField },
        geometryType,
        fields
      } = layer;

      const { features, globalIdFieldName, objectIdFieldName } = data;
      if (features && features.length) {
        features.forEach(feature => {
          const { attributes } = feature;
          const esriGeometry = feature.geometry;
          const globalId = attributes[globalIdFieldName];
          const localId = id + feature[objectIdFieldName];
          const featureId = globalId || localId;
          const featureTitle = attributes[displayField];
          const title = featureTitle || featureId;
          const coordinateAttributes = LayersObjectsApi.getCoordinateAttributes(
            geometryType,
            esriGeometry
          );
          const layersObjectGeometry = new LayersObjectGeometry({
            layerGeometryType: geometryType,
            coordinates: coordinateAttributes
          });
          const layerObjectAttributes = fields.map(field => {
            const value = attributes[field.name];
            return new LayersObjectAttribute(field, value);
          });

          const layersObject = new LayersObject({
            id: featureId,
            layerId: id,
            title,
            attributes: layerObjectAttributes,
            geometry: layersObjectGeometry
          });
          layersObjects.push(layersObject);
        });
      }
    });

    return layersObjects;
  }

  async getLayerObjects(layersObjectsSpecification) {
    const { features, layers } = layersObjectsSpecification;

    if (!layers || layers.length === 0) {
      return [];
    }

    const layersObjects = [];

    const listFeatureId = features.map(feature => feature.properties._id || 0);

    layers.map(layer => {
      const { data, displayField, id, geometryType, fields } = layer;
      let layerFeatures = [];
      if (data.features !== undefined) {
        layerFeatures = data.features;
      }
      const featureByLayer = layerFeatures.filter(item => {
        const {
          properties: { _id }
        } = item;
        return listFeatureId.includes(_id);
      });

      if (featureByLayer && featureByLayer.length) {
        featureByLayer.forEach(feature => {
          const { properties } = feature;
          const {
            properties: { _id: featureId }
          } = feature;
          const featureTitle = properties[displayField];
          const title = featureTitle || featureId;
          const coordinateAttributes = feature.geometry.coordinates;
          const layersObjectGeometry = new LayersObjectGeometry({
            layerGeometryType: geometryType,
            coordinates: coordinateAttributes
          });
          const layerObjectAttributes = fields.map(field => {
            const value = properties[field.name];
            return new LayersObjectAttribute(field, value);
          });

          const layersObject = new LayersObject({
            id: featureId,
            layerId: id,
            title,
            attributes: layerObjectAttributes,
            geometry: layersObjectGeometry
          });
          layersObjects.push(layersObject);
        });
      }
    });

    return layersObjects;
  }
}

export default LayersObjectsApi;
