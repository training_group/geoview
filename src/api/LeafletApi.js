export default class LeafletApi {
  static async init(id) {
    const gisApi = window.gisApi;
    const core = new gisApi.Core(id);
    window.LeafletApi = core;
    return new LeafletApi(core);
  }

  constructor(api) {
    this.api = api;
    this.baseMaps = {};
    this.eventHandlers = {};
    this.removedFeatures = [];
    this.addEventHandler = this.addEventHandler.bind(this);
    this.cancelTool = this.cancelTool.bind(this);
    this.interactionOff = this.interactionOff.bind(this);
  }

  addBaseLayers(baseLayers) {
    this.baseMaps = baseLayers.reduce(
      (acc, baseLayer) => ({ ...acc, [baseLayer.id]: baseLayer }),
      {}
    );
  }

  setCurrentBaseMap(basemapId) {
    const baseLayer = this.baseMaps[basemapId];
    const {
      data: { urlTemplate, options }
    } = baseLayer;
    this.api._map.setBaseMap({
      url: urlTemplate,
      ...options
    });
  }

  addLayer(layer) {
    const { data, visible, id } = layer;
    if (visible) {
      let vectorLayer = this.api.layer.createGeoJson(id);
      vectorLayer = this.api.layer.addGeoJson(id, data);
    }
  }

  updateLayers(layers) {
    const layersOnMap = this.api.layer.layers;
    layers.map(layer => {
      const { visible, id } = layer;
      if (!visible) {
        this.api.layer.remove(id);
      }
      if (visible && layersOnMap[id] === undefined) {
        this.addLayer(layer);
      }
    });
  }

  updateLayer(layer) {
    const { visible, id } = layer;
    this.api.layer.remove(id);
    if (visible) {
      this.addLayer(layer);
    }
  }

  addCluster(layerId, type, attributes, layers) {
    if (type === "#") {
      this.api.cluster.remove();
      this.addLayer(layers[layerId]);
    } else {
      const features = layers[layerId].data.features || [];
      this.api.layer.remove(layerId);
      this.api.cluster.init(layerId, type, features, attributes);
    }
  }

  removeCluster(layerId, layers) {
    this.api.cluster.remove(layerId);
    this.addLayer(layers[layerId]);
  }

  zoomTo(type, geometryData) {}

  setIdentification(identification) {
    this.cancelTool();

    const select = e => {
      const feature = e.sourceTarget.toGeoJSON();
      identification([feature]);
    };

    this.api.interaction.select.init(select);
  }

  stopIdentification() {
    this.api.layer.select();
    this.api.interaction.select.off();
  }

  draw(layerId, toolId, callback) {
    this.cancelTool();
    this.api.interaction.select.off();

    const vectorLayer = this.api.layer.get(layerId);
    const type = {
      drawPoint: "Marker",
      drawLine: "Polyline",
      drawPolygon: "Polygon",
      drawRectangular: "Rectangle"
    };
    this.api.interaction.draw.init(type[toolId], vectorLayer, callback);
    this.addEventHandler("draw", layerId);
  }

  drag(layerId) {
    this.cancelTool();
    this.api.interaction.select.off();

    const vectorLayer = this.api.layer.get(layerId);
    this.api.interaction.drag.on(vectorLayer);
    this.addEventHandler("drag", layerId);
  }

  modify(layerId) {
    this.cancelTool();
    this.api.interaction.select.off();

    const vectorLayer = this.api.layer.get(layerId);
    this.api.interaction.modify.on(vectorLayer);
    this.addEventHandler("modify", layerId);
  }

  remove(layerId) {
    const rm = e => {
      const feature = e.sourceTarget.toGeoJSON();
      const features = this.getFeaturesByLayerId.call(this, layerId);
      const hasLayer = features.findIndex(
        f => f.properties._id === feature.properties._id
      );
      if (hasLayer !== -1) {
        e.sourceTarget.remove();
        this.removedFeatures.push(feature.properties._id);
      }
    };

    this.api.interaction.select.init(rm);
  }

  removeFeatures() {
    const list = this.removedFeatures.slice();
    this.removedFeatures = [];
    return list;
  }

  addEventHandler(toolId, layerId) {
    if (this.eventHandlers[toolId] !== undefined) {
      this.eventHandlers[toolId].push(layerId);
    } else {
      this.eventHandlers[toolId] = [layerId];
    }
  }

  intersection(layerId, toolId, callback) {
    this.cancelTool();
    this.api.interaction.select.off();
    this.api.interaction.intersection.init(callback);
    this.addEventHandler("intersection", layerId);
  }

  cancelTool(toolId) {
    if (toolId === undefined) {
      Object.keys(this.eventHandlers).map(event => {
        if (event === "draw") {
          this.api.interaction[event].stop();
          this.eventHandlers = [];
        } else {
          this.interactionOff(event);
        }
      });
    }
    if (this.eventHandlers[toolId] !== undefined) {
      this.interactionOff(toolId);
    }
  }

  interactionOff(toolId) {
    this.eventHandlers[toolId] = this.eventHandlers[toolId].filter(layerId => {
      const vectorLayer = this.api.layer.get(layerId);
      if (this.api.interaction[toolId].off) {
        this.api.interaction[toolId].off(vectorLayer);
        return false;
      }
      return true;
    });
  }

  getFeaturesByLayerId(layerId) {
    const vectorLayer = this.api.layer.get(layerId);
    if (vectorLayer) {
      const { features } = vectorLayer.toGeoJSON();
      return features || [];
    }
    return [];
  }
}
