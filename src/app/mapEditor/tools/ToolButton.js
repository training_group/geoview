import React from 'react';
import Fab from '@material-ui/core/Fab';
import { withStyles } from '@material-ui/core/styles';


const stylesMui = () => ({
  rootPressed: {
    margin: '8px',
  },
  root: {
    margin: '8px',
    backgroundColor: '#3D3E40',
    color: '#fff',
  },
});

const ToolButton = ({
  id, onClick, children, pressed, classes,
}) => {
  const fabClasses = {
    root: pressed ? classes.rootPressed : classes.root,
  };

  const onClickButton = () => {
    if (onClick) {
      onClick(id);
    }
  };

  return (
    <Fab
      id={id}
      classes={fabClasses}
      size="medium"
      onClick={onClickButton}
    >
      { children }
    </Fab>
  );
};

export default withStyles(stylesMui)(ToolButton);
