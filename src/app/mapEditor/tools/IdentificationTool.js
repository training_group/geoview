import React from "react";
import ToolButton from "./ToolButton";
import addMapEditorPropsConnector from "../store/mapEditorPropsConnector";
import IdentificationIcon from "../../common/icons/identificationIcon";
import constants from "../store/mapEditorConstants";

const IdentificationTool = ({ currentToolId, actions }) => {
  const pressed = currentToolId === constants.IDENTIFICATION_TOOL_ID;
  return (
    <ToolButton
      id={constants.IDENTIFICATION_TOOL_ID}
      onClick={actions.setIdentificationTool}
      pressed={pressed}
    >
      <IdentificationIcon />
    </ToolButton>
  );
};

export default addMapEditorPropsConnector(IdentificationTool);
