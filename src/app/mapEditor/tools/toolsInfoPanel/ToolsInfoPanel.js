import React from 'react';
import styles from './ToolsInfoPanel.module.css';

const ToolsInfoPanel = ({ children }) => {
  return (
    <div className={styles.wrapper}>
      { children }
    </div>
  );
};


export default ToolsInfoPanel;
