import React from "react";
import EditIcon from "@material-ui/icons/Edit";
import IconSwitcherButton from "../../common/IconSwitcherButton/IconSwitcherButton";
import ItemSettingsIcon from "../../common/icons/ItemSettingsIcon";
import constants from "../store/mapEditorConstants";

const ProjectSettingsTool = ({ buttonClasses, iconClasses, onClick, on }) => {
  const onClickTool = () => {
    if (onClick) {
      onClick(constants.PROJECTS_EDITING_TOOL_ID);
    }
  };

  return (
    <IconSwitcherButton
      buttonClasses={buttonClasses}
      iconOn={<EditIcon classes={iconClasses} />}
      iconOff={<ItemSettingsIcon classes={iconClasses} />}
      on={on}
      onClick={onClickTool}
    />
  );
};

export default ProjectSettingsTool;
