import React from "react";
import ToolButton from "./ToolButton";
import addMapEditorPropsConnector from "../store/mapEditorPropsConnector";
import RulerIcon from "../../common/icons/RulerIcon";
import constants from "../store/mapEditorConstants";

const MeasurmentTool = ({ currentToolId, actions }) => {
  const pressed = currentToolId === constants.MESURMENT_TOOL_ID;
  return (
    <ToolButton
      id={constants.MESURMENT_TOOL_ID}
      onClick={actions.setMesurmentTool}
      pressed={pressed}
    >
      <RulerIcon />
    </ToolButton>
  );
};

export default addMapEditorPropsConnector(MeasurmentTool);
