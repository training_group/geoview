import React from "react";
import GrabIcon from "@material-ui/icons/PanToolOutlined";
import ToolButton from "./ToolButton";
import addMapEditorPropsConnector from "../store/mapEditorPropsConnector";
import constants from "../store/mapEditorConstants";

const HandTool = ({ currentToolId, actions }) => {
  const pressed = currentToolId === constants.HAND_TOOL_ID;
  return (
    <ToolButton
      id={constants.HAND_TOOL_ID}
      onClick={actions.setHandTool}
      pressed={pressed}
    >
      <GrabIcon />
    </ToolButton>
  );
};

export default addMapEditorPropsConnector(HandTool);
