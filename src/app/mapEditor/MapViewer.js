import React, { Component } from "react";
import PropTypes from "prop-types";
// import './ol.css';
import styles from "./MapViewer.module.css";

const containerId = "containerId";

class MapViewer extends Component {
  componentDidMount() {
    const { initViewer } = this.props;
    initViewer(containerId);
  }

  shouldComponentUpdate() {
    return false;
  }

  render() {
    return <div id={containerId} className={styles.viewer} />;
  }
}

MapViewer.propTypes = {
  initViewer: PropTypes.func.isRequired
};

export default MapViewer;
