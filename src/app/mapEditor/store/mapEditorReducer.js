import constants from "./mapEditorConstants";

const mapEditor = {
  viewers: [],
  viewer: undefined,
  currentToolId: undefined,
  isToolInfoPanelOpen: false,
  statusText: "",
  error: ""
};

const mapEditorReducer = (state = mapEditor, action) => {
  const { payload } = action;
  switch (action.type) {
    case constants.INIT_VIEWER_REQUEST:
      return Object.assign({}, state, {
        statusText: "Инициализация редактора карты",
        error: ""
      });
    case constants.INIT_VIEWER_FAILURE:
      return Object.assign({}, state, {
        error: payload,
        statusText: "Не удалось инициализировать редактор карты"
      });
    case constants.INIT_VIEWER_SUCCESS:
      return Object.assign({}, state, {
        viewer: payload,
        statusText: "Редактор карты инициализирован",
        error: ""
      });
    case constants.SET_TOOL:
      return Object.assign({}, state, {
        currentToolId: payload
      });
    case constants.SET_TOOLINFO_VISIBILITY:
      return Object.assign({}, state, {
        isToolInfoPanelOpen: payload
      });
    default:
      return state;
  }
};

export default mapEditorReducer;
