import constants from "./mapEditorConstants";
import { editor } from "../../appServices";

function initViewer(id) {
  const onRequest = () => ({
    type: constants.INIT_VIEWER_REQUEST
  });
  const onSuccess = payload => ({
    type: constants.INIT_VIEWER_SUCCESS,
    payload
  });

  const onFailure = payload => ({
    type: constants.INIT_VIEWER_FAILURE,
    payload
  });

  return async dispatch => {
    dispatch(onRequest());
    let mapEditor;
    try {
      mapEditor = await editor.init(id);
      dispatch(onSuccess(mapEditor));
    } catch (e) {
      console.log(e);
      dispatch(onFailure(e));
    }
    return mapEditor;
  };
}

function setTool(toolId) {
  const onChangeTool = id => ({
    type: constants.SET_TOOL,
    payload: id
  });

  return dispatch => dispatch(onChangeTool(toolId));
}

function setToolInfoVisivility(visible) {
  const onChangeTool = panelVisibility => ({
    type: constants.SET_TOOLINFO_VISIBILITY,
    payload: panelVisibility
  });

  const action = onChangeTool(visible);

  return dispatch => dispatch(action);
}

export default {
  initViewer,
  setTool,
  setToolInfoVisivility
};
