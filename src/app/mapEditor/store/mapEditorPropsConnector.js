import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import layersActions from "../../layers/store/layersActions";
import mapEditorFunctions from "./mapEditorActions";

import identificationActions from "../../identification/store/identificationActions";

function addMapEditorProps(ComposedComponent) {
  class MapEditorProps extends Component {
    constructor(props) {
      super(props);
      this.actions = {
        initViewer: this.initViewer.bind(this),
        setBaseLayer: this.setBaseLayer.bind(this),
        setIdentificationTool: this.setIdentificationTool.bind(this),
        setHandTool: this.setHandTool.bind(this),
        setMesurmentTool: this.setMesurmentTool.bind(this),
        setClusterSettingsTool: this.setClusterSettingsTool.bind(this),
        setEditTool: this.setEditTool.bind(this),
        cancel: this.cancelToolAction.bind(this),
        finish: this.finishToolAction.bind(this),
        removeLastVertex: this.removeLastVertex.bind(this),
        removeFeatures: this.removeFeatures.bind(this),
        cluster: this.addCluster.bind(this),
        removeCluster: this.removeCluster.bind(this)
      };
      this.identificationToolAction = this.identificationToolAction.bind(this);
      this.getFeatures = this.getFeatures.bind(this);
      this.afterDraw = this.afterDraw.bind(this);
      this.setClusteringLayers = this.setClusteringLayers.bind(this);
    }

    setHandTool(toolId) {
      const { mapViewer, mapEditorActions } = this.props;
      this.cancelToolAction();
      mapEditorActions.setTool(toolId);
      mapEditorActions.setToolInfoVisivility(false);
    }

    setIdentificationTool(toolId) {
      const { mapViewer, mapEditorActions } = this.props;
      if (mapViewer) {
        this.cancelToolAction();
        mapViewer.setIdentification(this.identificationToolAction);
        mapEditorActions.setTool(toolId);
      }
    }

    setMesurmentTool(toolId) {
      const { mapViewer, mapEditorActions } = this.props;
      this.cancelToolAction();
      mapEditorActions.setTool(toolId);
    }

    setClusterSettingsTool(toolId, layerId) {
      const {
        currentLayerId,
        layersStoreActions,
        mapEditorActions
      } = this.props;
      if (currentLayerId === layerId) {
        layersStoreActions.setCurrentLayer(undefined);
        mapEditorActions.setTool(undefined);
        mapEditorActions.setToolInfoVisivility(false);
      } else {
        layersStoreActions.setCurrentLayer(layerId);
        mapEditorActions.setTool(toolId);
        mapEditorActions.setToolInfoVisivility(true);
      }
    }

    removeClusterSettingsTool(layerId) {
      const {
        currentLayerId,
        layersStoreActions,
        mapEditorActions
      } = this.props;
      this.removeCluster(layerId);
      layersStoreActions.setCurrentLayer(undefined);
      mapEditorActions.setTool(undefined);
      mapEditorActions.setToolInfoVisivility(false);
    }

    setEditTool(nameTool, itemId) {
      const { mapViewer, mapEditorActions, layersStoreActions } = this.props;
      const [layerId, toolId] = nameTool.split("&");
      layersStoreActions.setCurrentLayer(layerId);
      mapEditorActions.setTool(toolId);
      const type = {
        drawPoint: mapViewer.draw,
        drawLine: mapViewer.draw,
        drawPolygon: mapViewer.draw,
        drawRectangular: mapViewer.draw,
        drag: mapViewer.drag,
        edit: mapViewer.modify,
        remove: mapViewer.remove,
        intersection: mapViewer.intersection
      };
      const callback = {
        drawPoint: this.afterDraw.bind(this),
        drawLine: this.afterDraw.bind(this),
        drawPolygon: this.afterDraw.bind(this),
        drawRectangular: this.afterDraw.bind(this),
        intersection: this.afterIntersection.bind(this)
      };
      type[toolId].call(mapViewer, layerId, toolId, callback[toolId]);
    }

    afterIntersection() {
      const { mapViewer, mapEditorActions, layersStoreActions } = this.props;
      mapEditorActions.setTool(undefined);
      layersStoreActions.setCurrentLayer(undefined);
    }

    identificationToolAction(geometry) {
      const { mapEditorActions, identificationActions, layers } = this.props;
      mapEditorActions.setToolInfoVisivility(true);
      identificationActions.identify(geometry, layers);
    }

    async updateLayer(features) {
      const {
        currentLayerId,
        layers,
        mapViewer,
        layersStoreActions
      } = this.props;
      const updatedLayers = await layersStoreActions.updateLayer(
        currentLayerId,
        features,
        layers
      );
      if (mapViewer) {
        mapViewer.cancelTool();
        mapViewer.updateLayer(updatedLayers[currentLayerId]);
      }
    }

    getFeatures() {
      const { currentLayerId, mapViewer } = this.props;
      if (mapViewer) {
        const features = mapViewer.getFeaturesByLayerId(currentLayerId);
        this.updateLayer(features);
      }
    }

    afterDraw(feature) {
      console.log(feature);
      this.updateLayer([feature]);
      this.cancelToolAction();
    }

    cancelToolAction() {
      const { mapViewer, mapEditorActions, layersStoreActions } = this.props;
      mapEditorActions.setTool(undefined);
      layersStoreActions.setCurrentLayer(undefined);
      // mapEditorActions.setToolInfoVisivility(false);
      if (mapViewer) {
        mapViewer.cancelTool();
      }
    }

    finishToolAction() {
      const {
        currentLayerId,
        mapViewer,
        mapEditorActions,
        layersStoreActions
      } = this.props;

      mapEditorActions.setTool(undefined);
      layersStoreActions.setCurrentLayer(undefined);
      if (mapViewer) {
        mapViewer.cancelTool();
        this.getFeatures();
      }
    }

    async removeFeatures() {
      const {
        currentLayerId,
        layers,
        mapViewer,
        mapEditorActions,
        layersStoreActions
      } = this.props;

      mapEditorActions.setTool(undefined);

      layersStoreActions.setCurrentLayer(undefined);
      if (mapViewer) {
        mapViewer.cancelTool();
        const features = mapViewer.removeFeatures();
        const updatedLayers = await layersStoreActions.removeFeatures(
          currentLayerId,
          features,
          layers
        );

        mapViewer.updateLayer(updatedLayers[currentLayerId]);
      }
    }

    removeLastVertex() {
      const { mapViewer } = this.props;
      //mapViewer.removeLastVertex();
    }

    addCluster(type, attributes) {
      const {
        currentLayerId,
        mapViewer,
        layers,
        layersStoreActions,
        mapEditorActions,
        clusteringLayers
      } = this.props;

      if (clusteringLayers.indexOf(currentLayerId) === -1 && mapViewer) {
        this.setClusteringLayers(currentLayerId);
        mapViewer.addCluster(currentLayerId, type, attributes, layers);
      }

      if (clusteringLayers.indexOf(currentLayerId) !== -1 && mapViewer) {
        mapViewer.removeCluster(currentLayerId, layers);
        mapViewer.addCluster(currentLayerId, type, attributes, layers);
      }

      layersStoreActions.setCurrentLayer(undefined);
      mapEditorActions.setTool(undefined);
      mapEditorActions.setToolInfoVisivility(false);
    }

    removeCluster(layerId) {
      const {
        layers,
        mapViewer,
        layersStoreActions,
        mapEditorActions
      } = this.props;
      if (mapViewer) {
        this.setClusteringLayers(layerId);
        mapViewer.removeCluster(layerId, layers);
      }
      layersStoreActions.setCurrentLayer(undefined);
      mapEditorActions.setTool(undefined);
      mapEditorActions.setToolInfoVisivility(false);
    }

    setClusteringLayers(layerId) {
      const { clusteringLayers, layersStoreActions } = this.props;

      const selectedIndex = clusteringLayers.indexOf(layerId);
      let newSelected = [];

      if (selectedIndex === -1) {
        newSelected = newSelected.concat(clusteringLayers, layerId);
      } else if (selectedIndex === 0) {
        newSelected = newSelected.concat(clusteringLayers.slice(1));
      } else if (selectedIndex === clusteringLayers.length - 1) {
        newSelected = newSelected.concat(clusteringLayers.slice(0, -1));
      } else if (selectedIndex > 0) {
        newSelected = newSelected.concat(
          clusteringLayers.slice(0, selectedIndex),
          clusteringLayers.slice(selectedIndex + 1)
        );
      }
      layersStoreActions.setClusteringLayers(newSelected);
    }

    setBaseLayer(id) {
      const { mapViewer, layersStoreActions } = this.props;
      mapViewer.editor.setCurrentBaseMap(id);
      layersStoreActions.updateBaseLayer(id);
    }

    async initViewer(id) {
      const { layersStoreActions, mapEditorActions } = this.props;

      const { baseLayers, layers } = await layersStoreActions.getLayers(
        "testUser"
      );
      // Если не получены вызывать ошибку инициализации
      const mapViewer = await mapEditorActions.initViewer(id);
      if (mapViewer) {
        mapViewer.addBaseLayers(baseLayers);
        mapViewer.addLayers(layers);
      }
    }

    render() {
      const { children, ...other } = this.props;

      return (
        <ComposedComponent actions={this.actions} {...other}>
          {children}
        </ComposedComponent>
      );
    }
  }

  function mapStateToProps(state) {
    return {
      account: state.account,
      mapViewer: state.mapEditor.viewer,
      currentToolId: state.mapEditor.currentToolId,
      isToolInfoPanelOpen: state.mapEditor.isToolInfoPanelOpen,
      layers: state.layers.accountLayers,
      baseLayers: state.layers.baseLayers,
      currentBaseLayerId: state.layers.currentBaseLayerId,
      currentLayerId: state.layers.currentLayerId,
      clusteringLayers: state.layers.clusteringLayers,
      initError: state.mapEditor.error
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
      layersStoreActions: bindActionCreators(layersActions, dispatch),
      mapEditorActions: bindActionCreators(mapEditorFunctions, dispatch),
      identificationActions: bindActionCreators(identificationActions, dispatch)
    };
  }

  return connect(mapStateToProps, mapDispatchToProps)(MapEditorProps);
}

export default addMapEditorProps;
