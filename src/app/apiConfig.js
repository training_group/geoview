const host = "localhost";
const port = 8700;
const src = 4326;
const config = {
  apiConfig: `http://${host}:${port}/`,
  src
};
export default config;
