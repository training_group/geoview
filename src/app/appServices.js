import AccountService from "../core/account/service/accountService";
import LayersApi from "../api/layersApi";
import LayersService from "../core/layers/service/layersService";

import config from "./apiConfig";

import OpenLayersApi from "../api/OpenLayersApi";
import LeafletApi from "../api/LeafletApi";
import MapEditor from "../core/mapEditor/MapEditor";
import LayersObjectsService from "../core/layersObjects/service/layersObjectsService";
import MapEditorService from "../core/mapEditor/service/mapEditorService";
import LayersObjectsApi from "../api/layersObjectsApi";
import IdentificationService from "../core/identification/service/identificationService";

const accountService = new AccountService();
const layersApi = new LayersApi(config);
const layersService = new LayersService(layersApi);

const layersObjectsRepository = new LayersObjectsApi();
const layersObjectsService = new LayersObjectsService(layersObjectsRepository);
const mapEditorService = new MapEditorService(layersObjectsService);
// const editor = new MapEditor({ api: OpenLayersApi, service: mapEditorService });
const editor = new MapEditor({ api: LeafletApi, service: mapEditorService });
const identificationService = new IdentificationService(layersObjectsService);

export { accountService, layersService, editor, identificationService };
