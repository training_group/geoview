import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Close from '@material-ui/icons/Clear';
import styles from './IdentificationPanel.module.css';


const stylseMui = () => ({
  rootButton: {
    padding: '0px',
  },
  rootIcon: {
    fontSize: '18px',
    fill: '#CCCCCC',
  },
  backButtonRoot: {
    color: '#ffffff',
    textDecoration: 'underline',
    textUnderlinePosition: 'under',
    padding: 0,
    fontSize: '10px',
    textTransform: 'initial',
    borderRadius: 0,
    lineHeight: 'initial',
    height: '24px',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '95px',
  },
});


const IdentificationPanel = ({
  classes, currentItem, statusText, onClose, children,
}) => {
  const buttonClasses = {
    root: classes.rootButton,
  };

  const iconClasses = {
    root: classes.rootIcon,
  };

  const backButtonClasses = {
    root: classes.backButtonRoot,
  };

  return (
    <div className={styles.wrapper}>
      <div className={styles.header}>
        <div className={styles.title}> Идентификация </div>
        <IconButton classes={buttonClasses} onClick={onClose}>
          <Close classes={iconClasses} />
        </IconButton>
      </div>
      <div className={styles.subHeader}>
        <div className={styles.statusText}>
          { statusText }
        </div>
      </div>
      <div className={styles.body}>
        { children }
      </div>
    </div>
  );
};


export default withStyles(stylseMui)(IdentificationPanel);
