import React from 'react';
import DomainNavigatorControl from '../common/domainNavigatorControl/DomainNavigatorControl';

const IdentificationNavigatorControl = ({ text }) => <DomainNavigatorControl title={text} />;

export default IdentificationNavigatorControl;
