const IDENTIFY_LAYERS_OBJECTS_REQUEST = 'IDENTIFY_LAYERS_OBJECTS_REQUEST';
const IDENTIFY_LAYERS_OBJECTS_SUCCESS = 'IDENTIFY_LAYERS_OBJECTS_SUCCESS';
const IDENTIFY_LAYERS_OBJECTS_FAILURE = 'IDENTIFY_LAYERS_OBJECTS_FAILURE';


const ON_SET_CURRENT_OBJECT = 'ON_SET_CURRENT_OBJECT';


export default {
  IDENTIFY_LAYERS_OBJECTS_REQUEST,
  IDENTIFY_LAYERS_OBJECTS_SUCCESS,
  IDENTIFY_LAYERS_OBJECTS_FAILURE,
  ON_SET_CURRENT_OBJECT,
};
