import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import identificationActions from "./identificationActions";
import mapEditorFunctions from "../../mapEditor/store/mapEditorActions";

function identificationPropsConnector(ComposedComponent) {
  class IdentificationProps extends PureComponent {
    constructor(props) {
      super(props);
      this.setCurrentItem = this.setCurrentItem.bind(this);
      this.closeItem = this.closeItem.bind(this);
      this.setNextItem = this.setNextItem.bind(this);
      this.setPrevItem = this.setPrevItem.bind(this);
      this.closePanel = this.closePanel.bind(this);
    }

    closePanel() {
      const { viewer, mapEditorActions } = this.props;

      if (viewer) {
        viewer.stopIdentification();
      }
      mapEditorActions.setTool(undefined);
      mapEditorActions.setToolInfoVisivility(false);
    }

    setCurrentItem(id) {
      const { items, identification } = this.props;
      items.forEach(item => {
        if (id === item.id) {
          identification.selectItem(item);
        }
      });
    }

    setNextItem() {
      const { items, currentItem, identification } = this.props;
      let currentIndex;

      items.forEach((item, index) => {
        if (currentItem.id === item.id) {
          currentIndex = index;
        }
      });

      if (currentIndex !== undefined) {
        const nextItem =
          currentIndex + 1 === items.length
            ? items[0]
            : items[currentIndex + 1];
        identification.selectItem(nextItem);
      }
    }

    setPrevItem() {
      const { items, currentItem, identification } = this.props;
      let currentIndex;

      items.forEach((item, index) => {
        if (currentItem.id === item.id) {
          currentIndex = index;
        }
      });

      if (currentIndex !== undefined) {
        const nextItem =
          currentIndex === 0
            ? items[items.length - 1]
            : items[currentIndex - 1];
        identification.selectItem(nextItem);
      }
    }

    closeItem() {
      const { identification } = this.props;
      identification.selectItem();
    }

    render() {
      const { children, ...other } = this.props;

      return (
        <ComposedComponent
          {...other}
          setCurrentItem={this.setCurrentItem}
          closeItem={this.closeItem}
          setNextItem={this.setNextItem}
          setPrevItem={this.setPrevItem}
          closePanel={this.closePanel}
        >
          {children}
        </ComposedComponent>
      );
    }
  }

  IdentificationProps.propTypes = {
    children: PropTypes.node
  };

  IdentificationProps.defaultProps = {
    children: undefined
  };

  function mapStateToProps(state) {
    return {
      ...state.identification,
      viewer: state.mapEditor.viewer
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
      identification: bindActionCreators(identificationActions, dispatch),
      mapEditorActions: bindActionCreators(mapEditorFunctions, dispatch)
    };
  }

  return connect(mapStateToProps, mapDispatchToProps)(IdentificationProps);
}

export default identificationPropsConnector;
