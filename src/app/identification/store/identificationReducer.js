import constants from './identificationConstants';

const identification = {
  currentItem: undefined,
  items: [],
  statusText: '',
  inProgress: false,
  error: '',
};

const identificationReducer = (state = identification, action) => {
  const { payload } = action;
  switch (action.type) {
    case constants.IDENTIFY_LAYERS_OBJECTS_REQUEST:
      return Object.assign({}, state, { statusText: 'Идет идентификация объектов', error: '', inProgress: true });
    case constants.IDENTIFY_LAYERS_OBJECTS_FAILURE:
      return Object.assign({}, state, {
        currentItem: undefined,
        error: payload,
        statusText: 'Не удалось идентифицировать объекты',
        items: [],
        inProgress: false,
      });
    case constants.IDENTIFY_LAYERS_OBJECTS_SUCCESS:
      return Object.assign({}, state, {
        currentItem: undefined,
        items: payload,
        statusText: `Идентифицировано объектов - ${payload && payload.length ? payload.length : 0}`,
        error: '',
        inProgress: false,
      });
    case constants.ON_SET_CURRENT_OBJECT:
      return Object.assign({}, state, {
        currentItem: payload,
      });
    default:
      return state;
  }
};

export default identificationReducer;
