import constants from "./identificationConstants";
import { identificationService } from "../../appServices";

function identify(geometry, layers) {
  const onRequest = () => ({
    type: constants.IDENTIFY_LAYERS_OBJECTS_REQUEST
  });
  const onSuccess = payload => ({
    type: constants.IDENTIFY_LAYERS_OBJECTS_SUCCESS,
    payload
  });

  const onFailure = payload => ({
    type: constants.IDENTIFY_LAYERS_OBJECTS_FAILURE,
    payload
  });

  return async dispatch => {
    dispatch(onRequest());
    let identificationItems = [];
    try {
      console.log(geometry, layers);
      identificationItems = await identificationService.identify(
        geometry,
        layers
      );
      dispatch(onSuccess(identificationItems));
    } catch (e) {
      console.log(e);
      dispatch(onFailure(e));
    }
    return identificationItems;
  };
}

function selectItem(identificationItem) {
  const onSet = () => ({
    type: constants.ON_SET_CURRENT_OBJECT,
    payload: identificationItem
  });
  return dispatch => dispatch(onSet());
}

export default {
  identify,
  selectItem
};
