import React from 'react';
import DomainObjectFormTitle from '../../common/domainObjectForm/DomainObjectFormTitle';

const IdentificationListItem = ({ title, subtitle, ...props }) => (<DomainObjectFormTitle title={title} subtitle={subtitle} {...props} />);

export default IdentificationListItem;
