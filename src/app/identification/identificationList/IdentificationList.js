import React from 'react';
import List from '@material-ui/core/List';

const IdentificationList = ({ className, children }) => (
  <List className={className} component="nav">
    {
      children
    }
  </List>
);


export default IdentificationList;
