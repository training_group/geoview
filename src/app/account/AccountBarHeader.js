import React from 'react';
import { withStyles } from '@material-ui/core/styles';


const stylesMui = () => ({
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    height: '44px',
    justifyContent: 'flex-end',
    flex: 1,
  },
  title: {
    display: 'flex',
    flexDirection: 'column',
  },
  role: {
    fontSize: '10px',
  },
  toolbar: {
    display: 'flex',
  },
});


const AccountBarHeader = ({
  classes, username, role, children,
}) => (
  <div className={classes.wrapper}>
    <div className={classes.title}>
      <span >{username}</span>
      <span className={classes.role}>
        {`Роль:  ${role}`}
      </span>
    </div>
    <div className={classes.toolbar}>
      {children}
    </div>
  </div>
);

export default withStyles(stylesMui)(AccountBarHeader);
