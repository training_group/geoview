import constants from './accountConstants';
import Account from '../../../core/account/model/account';

const account = new Account();

const accountReducer = (state = account, action) => {
  switch (action.type) {
    case constants.SIGNIN_SUCCESS:
      return Object.assign({}, state, action.payload);
    default:
      return state;
  }
};

export default accountReducer;
