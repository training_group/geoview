import constants from "./accountConstants";
import { accountService } from "../../appServices";

function signIn(accountData) {
  const onRequest = () => ({
    type: constants.SIGNIN_REQUEST
  });
  const onSuccess = payload => ({
    type: constants.SIGNIN_SUCCESS,
    payload
  });

  const onFailure = payload => ({
    type: constants.SIGNIN_FAILURE,
    payload
  });

  return dispatch => {
    dispatch(onRequest());
    return accountService
      .signIn(accountData)
      .then(account => dispatch(onSuccess(account)))
      .catch(e => {
        dispatch(onFailure(e));
      });
  };
}

export default {
  signIn
};
