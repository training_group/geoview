import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';


function addAccountProps(ComposedComponent) {
  class AccountProps extends Component {
    render() {
      const { children, ...other } = this.props;

      return (
        <ComposedComponent
          {...other}
        >
          {children}
        </ComposedComponent>
      );
    }
  }

  function mapStateToProps(state) {
    return {
      account: state.account,
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
      accountMethods: undefined,
    };
  }

  return connect(mapStateToProps, mapDispatchToProps)(AccountProps);
}

export default addAccountProps;
