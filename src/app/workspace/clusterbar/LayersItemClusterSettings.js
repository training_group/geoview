import React from "react";
import Settings from "@material-ui/icons/Tune";
import constants from "../../mapEditor/store/mapEditorConstants";
import styles from "./ListLayerCluster.module.css";

const LayersItemClusterSettings = ({
  id,
  currentLayerId,
  visible,
  onClick
}) => {
  const onClickItem = ({ currentTarget: { id } }) => {
    if (onClick) {
      onClick(constants.CLUSTER_TOOL_ID, id);
    }
  };

  return visible ? (
    <Settings
      id={id}
      className={id === currentLayerId ? styles.active : styles.icons}
      onClick={onClickItem}
    />
  ) : null;
};

export default LayersItemClusterSettings;
