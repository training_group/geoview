import React from "react";
import mapEditorPropsConnector from "../../mapEditor/store/mapEditorPropsConnector";
import ProgressBar from "../../common/progressBar/ProgressBar";
import ListLayerCluster from "./ListLayerCluster";

const WorkspaceClusterBar = ({ mapViewer, actions }) => {
  if (!mapViewer) {
    return <ProgressBar message="Внимание! Идет загрузка доступных ресурсов" />;
  }
  return <ListLayerCluster />;
};

export default mapEditorPropsConnector(WorkspaceClusterBar);
