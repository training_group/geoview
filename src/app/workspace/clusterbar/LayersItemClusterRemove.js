import React from "react";
import Delete from "@material-ui/icons/DeleteSweep";
import styles from "./ListLayerCluster.module.css";

const LayersItemClusterRemove = ({ id, currentLayerId, visible, onClick }) => {
  const onClickItem = ({ currentTarget: { id } }) => {
    if (onClick) {
      onClick(id);
    }
  };

  return visible ? (
    <Delete
      id={id}
      className={id === currentLayerId ? styles.active : styles.icons}
      onClick={onClickItem}
    />
  ) : null;
};

export default LayersItemClusterRemove;
