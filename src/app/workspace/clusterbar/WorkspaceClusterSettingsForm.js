import React from "react";
import mapEditorPropsConnector from "../../mapEditor/store/mapEditorPropsConnector";
import ContentSidebarHeaderToolbar from "../contentSidebar/ContentSidebarHeaderToolbar";
import DomainForm from "../../common/form/DomainForm";
import DomainFormMultiSelect from "../../common/form/DomainFormMultiSelect";
import DomainFormFieldSelect from "../../common/form/DomainFormFieldSelect";
import styles from "./ListLayerCluster.module.css";
import TYPE_CLUSTER from "./TYPE_CLUSTER";

const WorkspaceClusterSettingsForm = ({
  currentLayerId,
  layers,
  actions: { cluster }
}) => {
  const onSave = data => {
    const { typeCluster, fields } = data;
    const attributes = Object.keys(fields).filter(key => fields[key]);
    cluster(typeCluster, attributes);
  };

  const getNumberFields = () => {
    return layers[currentLayerId].fields.filter(field => {
      const { type } = field;
      return type === "number";
    });
  };

  const numberFields = getNumberFields();

  const initialValues = () => {
    const fields = numberFields.reduce((acc, numberField) => {
      return { ...acc, [numberField.name]: false };
    }, {});
    return {
      typeCluster: "default",
      fields
    };
  };

  return (
    <div className={styles.sidebar}>
      <div className={styles.header}>
        <ContentSidebarHeaderToolbar title="Настройки кластеризации" />
      </div>
      <div>
        {layers[currentLayerId] ? (
          <DomainForm initialValues={initialValues()} onSave={onSave}>
            <DomainFormFieldSelect
              name="typeCluster"
              alias="Тип кластеризации"
              options={TYPE_CLUSTER}
            />
            {numberFields.length !== 0 ? (
              <DomainFormMultiSelect
                name="fields"
                alias="Атрибуты"
                items={numberFields}
              />
            ) : (
              <div className={styles.wrapper}>
                <div className={styles.message}>
                  Внимание! Данный слой не имеет поля с количественными
                  значениями.
                </div>
              </div>
            )}
          </DomainForm>
        ) : null}
      </div>
    </div>
  );
};

export default mapEditorPropsConnector(WorkspaceClusterSettingsForm);
