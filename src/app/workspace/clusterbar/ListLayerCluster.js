import React from "react";
import { DomainList, DomainListItem } from "../../common/domainList";
import mapEditorPropsConnector from "../../mapEditor/store/mapEditorPropsConnector";
import LayersItemClusterSettings from "./LayersItemClusterSettings";
import LayersItemClusterRemove from "./LayersItemClusterRemove";

const ListLayerCluster = ({
  clusteringLayers,
  layers,
  currentLayerId,
  actions: { setClusterSettingsTool, removeCluster }
}) => {
  const createLayers = items => {
    return Object.keys(items)
      .filter(layerId => {
        const { geometryType } = items[layerId];
        return geometryType === "point";
      })
      .map(layerId => {
        const { id, title, visible, geometryType, editable } = items[layerId];

        return (
          <DomainListItem key={id} title={title}>
            <LayersItemClusterSettings
              id={id}
              currentLayerId={currentLayerId}
              visible={visible}
              onClick={setClusterSettingsTool}
            />
            {clusteringLayers.indexOf(layerId) !== -1 && (
              <LayersItemClusterRemove
                id={id}
                currentLayerId={currentLayerId}
                visible={visible}
                onClick={removeCluster}
              />
            )}
          </DomainListItem>
        );
      });
  };

  return <DomainList>{createLayers(layers)}</DomainList>;
};

export default mapEditorPropsConnector(ListLayerCluster);
