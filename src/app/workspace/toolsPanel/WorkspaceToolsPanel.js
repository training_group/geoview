import React from "react";
import ToolsInfoPanel from "../../mapEditor/tools/toolsInfoPanel/ToolsInfoPanel";
import mapEditorPropsConnector from "../../mapEditor/store/mapEditorPropsConnector";
import WorkspaceIdentification from "../identification/WorkspaceIdentification";
import WorkspaceClusterSettingsForm from "../clusterbar/WorkspaceClusterSettingsForm";
import constants from "../../mapEditor/store/mapEditorConstants";

const tools = {
  [constants.IDENTIFICATION_TOOL_ID]: WorkspaceIdentification,
  [constants.CLUSTER_TOOL_ID]: WorkspaceClusterSettingsForm
};

const WorkspaceToolsPanel = ({ currentToolId }) => {
  const CurrentTool = tools[currentToolId];
  return (
    <ToolsInfoPanel>
      {CurrentTool !== undefined ? <CurrentTool /> : <div />}
    </ToolsInfoPanel>
  );
};

export default mapEditorPropsConnector(WorkspaceToolsPanel);
