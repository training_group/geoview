import React from 'react';
import { withStyles } from '@material-ui/core';
import IdentificationList from '../../identification/identificationList/IdentificationList';
import IdentificationListItem from '../../identification/identificationList/IdentificationListItem';

const stylesMui = {
  list: {
    padding: '0 12px 12px 12px',
    overflowY: 'auto',
  },
};

const WorkspaceIdentificationList = ({ classes, items, onClickItem }) => {
  const onClick = (id) => {
    if (onClickItem) {
      onClickItem(id);
    }
  };


  console.log('here')

  return (
    <IdentificationList className={classes.list}>
      { items.map((item) => {
        const { title, subTitle, id } = item;
        return (
          <IdentificationListItem
            button
            key={id}
            id={id}
            title={title}
            subtitle={subTitle}
            onClick={onClick}
          />
        );
      }) }
    </IdentificationList>
  );
};

export default withStyles(stylesMui)(WorkspaceIdentificationList);
