import React from "react";

import identificationPropsConnector from "../../identification/store/identificationPropsConnector";
import IdentificationPanel from "../../identification/IdentificationPanel";
import WorkspaceIdentificationList from "./WorkspaceIdentificationList";
import WorkspaceIdentificationItem from "./WorkspaceIdentificationItem";

//const currentItem = { id: 1, title: 'тестовый', subTitle: 'тестовый', identifiedObject: { attributes: { propertyName: '1' }, geometry: [], files: [], link: [], pictures: [] }};

const WorkspaceIdentification = ({
  items,
  currentItem,
  statusText,
  setCurrentItem,
  closeItem,
  setNextItem,
  setPrevItem,
  closePanel
}) => {
  return (
    <IdentificationPanel statusText={statusText} onClose={closePanel}>
      <WorkspaceIdentificationList items={items} onClickItem={setCurrentItem} />
    </IdentificationPanel>
  );
};

export default identificationPropsConnector(WorkspaceIdentification);
