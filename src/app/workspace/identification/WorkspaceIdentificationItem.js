import React, { Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import ArrowForward from '@material-ui/icons/KeyboardArrowRight';
import ArrowBack from '@material-ui/icons/KeyboardArrowLeft';
import Button from '@material-ui/core/Button';
import LayersObjectForm from '../../layersObjects/layersObjectForm/LayersObjectForm';
import IdentificationListItem from '../../identification/identificationList/IdentificationListItem';


const stylesMui = () => ({
  item: {
    backgroundColor: 'rgba(80, 80, 80, 0.7)',
    padding: '0 12px',
    boxSizing: 'border-box',
  },
  backButton: {
    textTransform: 'initial',
    margin: '0px 12px 12px 12px',
    color: '#f5f5f5',
  },
  divider: {
    backgroundColor: 'rgba(204, 204, 204, 0.5)',
  },
  verticalDivider: {
    backgroundColor: 'rgba(204, 204, 204, 0.5)',
    width: '1px',
    height: '14px',
  },
  toolbar: {
    height: '30px',
    backgroundColor: 'rgba(80, 80, 80, 0.7)',
    boxSizing: 'border-box',
    color: '#f5f5f5',
    marginBottom: '10px',
    boxShadow: '0 0 4px rgba(0, 0, 0, 0.25)',
    display: 'flex',
    justifyContent: 'space-between',
    minHeight: '30px',
  },
  buttonRoot: {
    fontSize: '11px',
    color: '#CCCCCC',
    textTransform: 'none',
  },
  iconRoot: {
    fontSize: '20px',
  },
  rightTools: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});


const WorkspaceIdentificationItem = ({ classes, item, close, setNext, setPrev }) => {
  const {
    id, title, subTitle, identifiedObject,
  } = item;

  const dividerClasses = {
    root: classes.divider,
  };

  const verticalDividerClasses = {
    root: classes.verticalDivider,
  };

  const buttonClasses = {
    root: classes.buttonRoot,
  };

  const iconRoot = {
    root: classes.iconRoot,
  };

  return (
    <Fragment>
      <div className={classes.toolbar}>
        <Button classes={buttonClasses} size="small" onClick={close}>
          <ArrowBack classes={iconRoot} />
          Назад к списку
        </Button>
        <div className={classes.rightTools}>
          <Button
            classes={buttonClasses}
            size="small"
            onClick={setPrev}
          >
            <ArrowBack classes={iconRoot} />
            Назад
          </Button>
          <Divider classes={verticalDividerClasses} />
          <Button
            classes={buttonClasses}
            size="small"
            onClick={setNext}
          >
            Вперед
            <ArrowForward classes={iconRoot} />
          </Button>
        </div>
      </div>
      <div className={classes.item}>
        <IdentificationListItem id={id} title={title} subtitle={subTitle} />
        <Divider classes={dividerClasses} />
      </div>
      <LayersObjectForm layersObject={identifiedObject} />
    </Fragment>
  );
};


export default withStyles(stylesMui)(WorkspaceIdentificationItem);
