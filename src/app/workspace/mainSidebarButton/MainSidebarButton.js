import React from 'react';
import styles from './MainSidebarButton.module.css';

const MainSidebarButton = ({ onClick, children }) => (
  <button
    type="button"
    className={styles.default}
    onClick={onClick}
  >
    { children }
  </button>
);

export default MainSidebarButton;
