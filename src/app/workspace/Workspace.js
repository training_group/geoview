import React, { Component } from "react";
import classNames from "classnames";
import { matchPath } from "react-router-dom";
import mapEditorPropsConnector from "../mapEditor/store/mapEditorPropsConnector";
import MapViewer from "../mapEditor/MapViewer";
import MainSidebar from "./mainSidebar/MainSidebar";
import MainToolbar from "./mainToolBar/MainToolbar";
import WorkspaceToolsPanel from "./toolsPanel/WorkspaceToolsPanel";
import ContentSidebar from "./contentSidebar/ContentSidebar";
import SecondarySidebar from "./secondarySidebar/SecondarySidebar";

import styles from "./Workspace.module.css";

const rootPath = "/";

class Workspace extends Component {
  constructor(props) {
    super(props);
    this.closeContentSidebar = this.closeContentSidebar.bind(this);
  }

  closeContentSidebar() {
    const { history } = this.props;
    history.push(rootPath);
  }

  render() {
    const {
      location,
      match,
      actions,
      currentToolId,
      isToolInfoPanelOpen,
      initError
    } = this.props;
    const { pathname } = location;
    const isContentActive = !matchPath(
      pathname,
      { path: rootPath, exact: true },
      match
    );
    const containerClasses = classNames({
      [styles.controlsContainer]: true,
      [styles.controlsContainerShifted]: isToolInfoPanelOpen
    });
    return (
      <div className={styles.workspacePage}>
        <div className={containerClasses}>
          <MainSidebar key="1" />
          {isContentActive && (
            <ContentSidebar close={this.closeContentSidebar} />
          )}
          <MainToolbar currentToolId={currentToolId} />
        </div>
        <MapViewer initViewer={actions.initViewer} />
        <SecondarySidebar isOpen={isToolInfoPanelOpen}>
          <WorkspaceToolsPanel />
        </SecondarySidebar>
      </div>
    );
  }
}

export default mapEditorPropsConnector(Workspace);
