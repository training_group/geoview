import React from "react";

import IdentificationTool from "../../mapEditor/tools/IdentificationTool";
import styles from "./MainToolbar.module.css";

const Toolbar = () => (
  <div className={styles.gorizontal}>
    <IdentificationTool />
  </div>
);

export default Toolbar;
