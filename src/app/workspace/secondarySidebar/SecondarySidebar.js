import React from 'react';
import { withStyles, Drawer } from '@material-ui/core';

const stylesMui = {
  paper: {
    top: '0px',
    bottom: 0,
    height: 'auto',
    borderLeft: 'none',
  },
};


const SecondarySidebar = ({ children, classes, isOpen }) => {
  return (
    <Drawer classes={classes} anchor="right" open={isOpen} variant="persistent">
      { children }
    </Drawer>
  );
};

export default withStyles(stylesMui)(SecondarySidebar);
