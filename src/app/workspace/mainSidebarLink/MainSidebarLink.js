import React from 'react';
import { NavLink } from 'react-router-dom';
import styles from './MainSidebarLink.module.css';


const MainSidebarLink = ({ to, exact, children }) => (
  <NavLink
    className={styles.default}
    activeClassName={styles.active}
    exact={exact}
    to={to}
  >
    { children }
  </NavLink>
);

export default MainSidebarLink;
