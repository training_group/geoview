import React from "react";
import { Route } from "react-router-dom";
import ContentSidebarHeaderToolbar from "./ContentSidebarHeaderToolbar";
import WorkspaceAccountBarHeader from "../accountbar/WorkspaceAccountBarHeader";

const ContentSidebarHeaderRouter = () => [
  <Route
    key="1"
    exact
    path="/workspace/account"
    render={() => <ContentSidebarHeaderToolbar title="Учетная запись" />}
  />,
  <Route
    key="2"
    exact
    path="/workspace/accountbar"
    render={() => <WorkspaceAccountBarHeader />}
  />,
  <Route
    key="8"
    exact
    path="/workspace/layers"
    render={() => <ContentSidebarHeaderToolbar title="Слои" />}
  />,
  <Route
    key="9"
    exact
    path="/workspace/cluster"
    render={() => <ContentSidebarHeaderToolbar title="Кластеризация" />}
  />,
  <Route
    key="3"
    exact
    path="/workspace/search"
    render={() => <ContentSidebarHeaderToolbar title="Поиск" />}
  />,
  <Route
    key="4"
    exact
    path="/workspace/basemaps"
    render={() => <ContentSidebarHeaderToolbar title="Подложки" />}
  />,
  <Route
    key="5"
    exact
    path="/workspace/outsource"
    render={() => <ContentSidebarHeaderToolbar title="Внешние источники" />}
  />,
  <Route
    key="6"
    exact
    path="/workspace/editing"
    render={() => <ContentSidebarHeaderToolbar title="Редактирование" />}
  />,
  <Route
    key="7"
    exact
    path="/workspace/print"
    render={() => <ContentSidebarHeaderToolbar title="Печать" />}
  />
];

export default ContentSidebarHeaderRouter;
