import React from 'react';
import styles from './ContentSidebar.module.css';


const ContentSidebarListWrapper = ({ children }) => (
  <div className={styles.listWrapper}>
    { children }
  </div>
);

export default ContentSidebarListWrapper;
