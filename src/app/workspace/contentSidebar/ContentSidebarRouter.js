import React from "react";
import { Route } from "react-router-dom";
import sidebarListWrapper from "../sidebarListWrapper";
import LayersList from "../../layers/layersList/LayersList";
import WorkspaceAccountBar from "../accountbar/WorkspaceAccountBar";
import WorkspaceClusterBar from "../clusterbar/WorkspaceClusterBar";

const WorkspaceEditing = sidebarListWrapper(LayersList);

const ContentSidebarRouter = () => [
  <Route
    key={1}
    exact
    path="/workspace/editing"
    component={WorkspaceEditing}
  />,
  <Route
    key={2}
    exact
    path="/workspace/accountbar"
    component={WorkspaceAccountBar}
  />,
  <Route
    key={3}
    exact
    path="/workspace/cluster"
    component={WorkspaceClusterBar}
  />
];

export default ContentSidebarRouter;
