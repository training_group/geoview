import React from "react";
import styles from "./ContentSidebar.module.css";

import ContentSidebarRouter from "./ContentSidebarRouter";
import ContentSidebarHeader from "./ContentSidebarHeader";
import ContentSidebarHeaderRouter from "./ContentSidebarHeaderRouter";

const ContentSidebar = ({ close }) => {
  return (
    <div className={styles.sidebar}>
      <ContentSidebarHeader
        onClose={close}
        title={<ContentSidebarHeaderRouter />}
      />
      <ContentSidebarRouter />
    </div>
  );
};

export default ContentSidebar;
