import React from 'react';
import { withStyles } from '@material-ui/core/styles';


const stylesMui = () => ({
  wrapper: {
    display: 'flex',
    alignItems: 'center',
  },
  iconRoot: {
    width: '24px !important',
    height: '24px !important',
    fill: '#CCCCCC !important',
  },
  iconButton: {
    padding: '8px',
    marginTop: '2px',
  },
});

const ContentSidebarHeaderToolbar = ({ classes, title, add }) => {

  return (
    <div className={classes.wrapper}>
      { title }
    </div>
  );
};

export default withStyles(stylesMui)(ContentSidebarHeaderToolbar);
