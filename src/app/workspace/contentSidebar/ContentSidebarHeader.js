import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import Close from '@material-ui/icons/Clear';

import styles from './ContentSidebar.module.css';

const classes = {
  root: styles.iconRoot,
};

const ContentSidebarHeader = ({ title, onClose }) => (
  <div className={styles.header}>
    { title }
    <div>
      <IconButton onClick={onClose}>
        <Close classes={classes} />
      </IconButton>
    </div>
  </div>
);


export default ContentSidebarHeader;
