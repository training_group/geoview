import React from "react";
import Drawer from "@material-ui/core/Drawer";
import { withStyles } from "@material-ui/core/styles";
import AccountIcon from "@material-ui/icons/AssignmentInd";
import EditingIcon from "@material-ui/icons/Edit";
import ClusterIcon from "@material-ui/icons/BubbleChart";

import MainSidebarLink from "../mainSidebarLink/MainSidebarLink";

import styles from "./MainSidebar.module.css";

const stylesMui = () => ({
  paperAnchorDockedLeft: {
    backgroundColor: "#3D3E40",
    top: "0px",
    bottom: "0",
    height: "auto"
  }
});

const Sidebar = ({ classes }) => (
  <Drawer
    open
    className={styles.vertical}
    classes={classes}
    variant="persistent"
  >
    <div className={styles.main}>
      <MainSidebarLink to="/workspace/accountbar">
        <AccountIcon className={styles.icon} />
      </MainSidebarLink>
      <MainSidebarLink to="/workspace/editing">
        <EditingIcon className={styles.icon} />
      </MainSidebarLink>
      <MainSidebarLink to="/workspace/cluster">
        <ClusterIcon className={styles.icon} />
      </MainSidebarLink>
    </div>
  </Drawer>
);

export default withStyles(stylesMui)(Sidebar);
