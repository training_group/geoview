import React from 'react';
import ContentSidebarListWrapper from './contentSidebar/ContentSidebarListWrapper';


export default function (ComposedComponent) {
  const WorkspaceWrapper = props => (
    <ContentSidebarListWrapper>
      <ComposedComponent {...props} />
    </ContentSidebarListWrapper>
  );
  return WorkspaceWrapper;
}
