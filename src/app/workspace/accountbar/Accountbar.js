import React from 'react';
import styles from './Accountbar.module.css';
import userIcon from '../../common/icons/user.svg';


const Accountbar = () => (
  <div className={styles.circle}>
    <img alt="user" src={userIcon} />
  </div>
);


export default Accountbar;
