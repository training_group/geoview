import React, { Component } from 'react';
import AccountBarHeader from '../../account/AccountBarHeader';
import accountPropsConnector from '../../account/store/accountPropsConnector';


class WorkspaceAccountBarHeader extends Component {
  shouldComponentUpdate(prevProps) {
    const { account } = this.props;
    const { username } = prevProps.account; 

    if (account.username !== username) {
      return true;
    }

    return false;
  }

  render() {
    const { account: { username, role } } = this.props;
    return (
      <AccountBarHeader username={username} role={role} />
    );
  }
}


export default accountPropsConnector(WorkspaceAccountBarHeader);
