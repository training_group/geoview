import React from "react";
import mapEditorPropsConnector from "../../mapEditor/store/mapEditorPropsConnector";
import ProgressBar from "../../common/progressBar/ProgressBar";
import BaseLayerSwitcher from "../../layers/baseLayer/BaseLayerSwitcher";

const WorkspaceAccountBar = ({
  currentBaseLayerId,
  mapViewer,
  actions,
  baseLayers
}) => {
  if (!mapViewer) {
    return <ProgressBar message="Внимание! Идет загрузка доступных ресурсов" />;
  }
  return (
    <BaseLayerSwitcher
      currentBaseLayerId={currentBaseLayerId}
      baseLayers={baseLayers}
      onSelect={actions.setBaseLayer}
    />
  );
};

export default mapEditorPropsConnector(WorkspaceAccountBar);
