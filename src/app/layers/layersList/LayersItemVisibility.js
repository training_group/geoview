import React from 'react';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import styles from './LayersList.module.css';


const LayersItemVisibility = ({ id, visible, onClick }) => {
  const onClickItem = ({ currentTarget: { id } }) => {
    if (onClick) {
      onClick(id);
    }
  };

  return visible
    ? <Visibility id={id} className={styles.icons} onClick={onClickItem} />
    : <VisibilityOff id={id} className={styles.icons} onClick={onClickItem} />;
};


export default LayersItemVisibility;
