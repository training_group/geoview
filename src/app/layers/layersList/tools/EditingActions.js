import MarkerIcon from "../../../common/icons/MarkerIcon";
import LineIcon from "../../../common/icons/LineIcon";
import PolygonIcon from "../../../common/icons/PolygonIcon";
import RectangleIcon from "../../../common/icons/RectangleIcon";
import EditIcon from "../../../common/icons/EditIcon";
import CutIcon from "../../../common/icons/CutIcon";
import DragIcon from "../../../common/icons/DragIcon";
import RemoveIcon from "../../../common/icons/RemoveIcon";
import IntersectionIcon from "../../../common/icons/IntersectionIcon";

const dictionary = {
  point: [{ name: "Точка", value: "drawPoint", icon: MarkerIcon }],
  line: [
    { name: "Линия", value: "drawLine", icon: LineIcon },
    { name: "Пересечение", value: "intersection", icon: IntersectionIcon }
  ],
  polygon: [
    { name: "Полигон", value: "drawPolygon", icon: PolygonIcon },
    { name: "Прямоугольник", value: "drawRectangular", icon: RectangleIcon },
    { name: "Пересечение", value: "intersection", icon: IntersectionIcon }
  ]
};

const EDITING_ACTIONS = [
  { name: "Редактировать", value: "edit", icon: EditIcon },
  { name: "Переместить", value: "drag", icon: DragIcon },
  // { name: "Вырезать", value: "cut", icon: CutIcon },
  { name: "Удалить", value: "remove", icon: RemoveIcon }
];

export { dictionary, EDITING_ACTIONS };
