import React, { Component } from "react";
import PropTypes from "prop-types";
import FormControl from "@material-ui/core/FormControl";
import FormGroup from "@material-ui/core/FormGroup";
import { withStyles } from "@material-ui/core/styles";

import muiStyles from "../../../common/domainRadioButtonGroup/DomainRadioButtonGroup.MuiStyles";

const ToolButtonGroup = ({
  layerId,
  value,
  values,
  currentLayerId,
  onChange,
  additionalActions,
  classes
}) => {
  return (
    <FormControl className={classes.formControl}>
      <FormGroup value={value} className={classes.radioGroup}>
        {values.map(valueItem => {
          const Icon = valueItem.icon;
          const iconStyle =
            valueItem.value === value && currentLayerId === layerId
              ? classes.active
              : classes.icons;
          return (
            <Icon
              id={valueItem.value}
              key={valueItem.value}
              className={iconStyle}
              onClick={event => onChange(`${layerId}&${valueItem.value}`)}
            />
          );
        })}
      </FormGroup>
      <FormGroup className={classes.formGroup}>
        {additionalActions.map((action, idx) => {
          return (
            <span
              id={action.value}
              key={action.value}
              onClick={action.click}
              className={classes.formLabel}
            >
              {action.name}
            </span>
          );
        })}
      </FormGroup>
    </FormControl>
  );
};

ToolButtonGroup.propTypes = {
  layerId: PropTypes.string,
  value: PropTypes.string,
  currentLayerId: PropTypes.string,
  values: PropTypes.arrayOf(PropTypes.any),
  onChange: PropTypes.func,
  additionalActions: PropTypes.arrayOf(PropTypes.objectOf(PropTypes.any)),
  classes: PropTypes.objectOf(PropTypes.any).isRequired
};

ToolButtonGroup.defaultProps = {
  additionalActions: []
};

export default withStyles(muiStyles)(ToolButtonGroup);
