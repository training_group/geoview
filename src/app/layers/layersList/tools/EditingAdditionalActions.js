export default ({ cancel, finish, removeLastVertex, removeFeatures }) => {
  const ADDITIONAL_ACTIONS = {
    drawPoint: [{ name: "Отменить", value: "cancel", click: cancel }],
    drawLine: [
      { name: "Отменить", value: "cancel", click: cancel },
      { name: "Завершить", value: "finish", click: cancel }
    ],
    drawPolygon: [
      { name: "Отменить", value: "cancel", click: cancel },
      { name: "Завершить", value: "finish", click: cancel }
    ],
    drawRectangular: [
      { name: "Отменить", value: "cancel", click: cancel },
      { name: "Завершить", value: "finish", click: cancel }
    ],
    drawCircle: [
      { name: "Отменить", value: "cancel", click: cancel },
      { name: "Завершить", value: "finish", click: cancel }
    ],
    edit: [
      {
        name: "Завершить",
        value: "finish",
        click: finish
      }
    ],
    drag: [
      {
        name: "Завершить",
        value: "finish",
        click: finish
      }
    ],
    remove: [
      {
        name: "Завершить",
        value: "finish",
        click: removeFeatures
      }
    ],
    intersection: [{ name: "Завершить", value: "finish", click: finish }]
  };
  return ADDITIONAL_ACTIONS;
};
