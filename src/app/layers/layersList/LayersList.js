import React from "react";
import { DomainList, DomainListItem } from "../../common/domainList";
import LayersItemVisibility from "./LayersItemVisibility";
import addLayersProps from "../store/layersPropsConnector";
import LayersItemToolbar from "./LayersItemToolbar";

const LayersList = ({ layers, actions: { toggleVisibility } }) => {
  const createLayers = items => {
    return Object.keys(items).map(layerId => {
      const { id, title, visible, geometryType, editable } = items[layerId];

      const toolbar = (
        <LayersItemToolbar
          id={id}
          visible={visible}
          geometryType={geometryType}
          editable={editable}
        />
      );

      return (
        <DomainListItem key={id} title={title} toolbar={toolbar}>
          <LayersItemVisibility
            id={id}
            visible={visible}
            onClick={toggleVisibility}
          />
        </DomainListItem>
      );
    });
  };

  return <DomainList>{createLayers(layers)}</DomainList>;
};

export default addLayersProps(LayersList);
