import React from "react";

import { dictionary, EDITING_ACTIONS } from "./tools/EditingActions";
import ToolButtonGroup from "./tools/ToolButtonGroup";
import EditingAdditionalActions from "./tools/EditingAdditionalActions";

import addMapEditorPropsConnector from "../../mapEditor/store/mapEditorPropsConnector";

const LayersItemToolbar = ({
  id,
  geometryType,
  visible,
  editable,
  actions,
  currentToolId,
  currentLayerId
}) => {
  const drawGeometryTools = dictionary[geometryType];

  let additionalActions = [];
  if (currentToolId !== undefined && currentLayerId === id) {
    additionalActions = EditingAdditionalActions(actions)[currentToolId] || [];
  }

  const values = [...drawGeometryTools, ...EDITING_ACTIONS];
  return visible && editable ? (
    <ToolButtonGroup
      layerId={id}
      values={values}
      value={currentToolId}
      currentLayerId={currentLayerId}
      onChange={actions.setEditTool}
      additionalActions={additionalActions}
    />
  ) : null;
};

export default addMapEditorPropsConnector(LayersItemToolbar);
