import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';


import { stylesCard } from './BaseLayers.styles';


const BaseLayerCard = ({ classes, name, imageSrc }) => {
  return (
    <Card className={classes.card}>
      <CardActionArea>
        <CardMedia
          component="img"
          alt={name}
          className={classes.media}
          height="140"
          src={imageSrc}
          title={name}
        />
        <div className={classes.cardFooter}>
          {name}
        </div>
      </CardActionArea>
    </Card>
  );
};

 BaseLayerCard.propTypes={
   name:PropTypes.string.isRequired,
   imageSrc:PropTypes.string.isRequired,
   classes: PropTypes.objectOf(PropTypes.any).isRequired,
 };

export default withStyles(stylesCard)(BaseLayerCard);
