const styles = () => ({
  wrapper: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    color: '#CCCCCC',
    fontSize: 13,
    lineHeight: '30px',
    marginLeft: '4px',
  },
  popper: {
    zIndex: 2,
  },
  icon: {
    fontSize: 14,
  },
  menuItemRoot: {
    width: '99px',
    height: '75px',
    boxSizing: 'border-box',
    marginBottom: '8px',
    padding: 'initial',
  },
  menuPaper: {
    display: 'flex',
    width: '126px',
    padding: '8px 0 8px 8px',
    backgroundColor: '#3D3E40',
    border: '1px solid #505050',
    boxSizing: 'border-box',
    boxShadow: '4px 4px 4px rgba(0, 0, 0, 0.25)',
    borderRadius: 0,
    overflowY: 'visible',
    maxHeight: '435px',
  },
  buttonRoot: {
    fontSize: '14px',
    padding: '8px',
    color: 'inherit',
    marginRight: '2px',
  },
  menuListPadding: {
    padding: 0,
    overflowY: 'auto',
    flex: 1,
  },
  grow: {
    overflowY: 'auto',
    overflowX: 'hidden',
    maxHeight: '435px',
  },
});
const stylesCard = {
  card: {
    width: '99px',
    height: '75px',
    borderRadius: 0,
  },
  media: {
    objectFit: 'cover',
    height: '75px',
  },
  cardContent: {
    width: '99px',
    height: '24px',
    background: 'rgba(80, 80, 80, 0.8)',
    boxSizing: 'border-box',
    borderRadius: 0,
  },
  cardFooter: {
    display: 'flex',
    top: '68%',
    position: 'absolute',
    width: '100%',
    height: '32%',
    background: '#292828cc',
    color: '#FFFFFF',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: '10px',
  },
};

export {
  styles,
  stylesCard
};