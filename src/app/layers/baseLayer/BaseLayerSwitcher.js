import React, { Component } from "react";
import PropTypes from "prop-types";

import { withStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import IconButton from "@material-ui/core/IconButton";
import Paper from "@material-ui/core/Paper";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import MenuList from "@material-ui/core/MenuList";
import Popover from "@material-ui/core/Popover";
import MapIcon from "@material-ui/icons/Map";

import BaseLayerCard from "./BaseLayerCard";

import { styles } from "./BaseLayers.styles";

class BaseLayerSwitcher extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false
    };

    this.handleToggle = this.handleToggle.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.close = this.close.bind(this);
  }

  handleToggle() {
    this.setState(state => ({ open: !state.open }));
  }

  close() {
    this.setState({ open: false });
  }

  handleClose({ target, currentTarget: { id } }) {
    if (this.anchorEl.contains(target)) {
      return;
    }

    const { onSelect } = this.props;

    this.setState({ open: false });
    if (onSelect) {
      onSelect(id);
    }
  }

  render() {
    const { open } = this.state;
    const { classes, currentBaseLayerId, baseLayers } = this.props;

    if (!baseLayers || baseLayers.length === 0) {
      return <div className={classes.wrapper}>Нет доступных базовых слоев</div>;
    }

    let baseLayerName = "Базовый слой не определен";

    baseLayers.forEach(baseLayer => {
      const { id, title } = baseLayer;
      if (id === currentBaseLayerId) {
        baseLayerName = title;
      }
    });

    const buttonClasses = {
      root: classes.buttonRoot
    };

    const menuItemClasses = {
      root: classes.menuItemRoot
    };

    const menuListClasses = {
      padding: classes.menuListPadding
    };

    const iconClasses = {
      root: classes.icon
    };

    return [
      <div key={1} className={classes.wrapper}>
        Базовый слой
      </div>,
      <div key={2} className={classes.wrapper}>
        <div>{baseLayerName}</div>
        <IconButton
          classes={buttonClasses}
          buttonRef={node => {
            this.anchorEl = node;
          }}
          aria-owns={open ? "menu-list-grow" : undefined}
          aria-haspopup="true"
          onClick={this.handleToggle}
        >
          <MapIcon classes={iconClasses} />
        </IconButton>
        <Popover
          open={open}
          anchorEl={this.anchorEl}
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "right"
          }}
          transformOrigin={{
            vertical: "top",
            horizontal: "left"
          }}
        >
          <Paper className={classes.menuPaper}>
            <ClickAwayListener onClickAway={this.close}>
              <MenuList classes={menuListClasses}>
                {baseLayers.map(map => {
                  const { id, title, previewUrl } = map;
                  return (
                    <MenuItem
                      key={id}
                      id={id}
                      classes={menuItemClasses}
                      onClick={this.handleClose}
                    >
                      <BaseLayerCard name={title} imageSrc={previewUrl} />
                    </MenuItem>
                  );
                })}
              </MenuList>
            </ClickAwayListener>
          </Paper>
        </Popover>
      </div>
    ];
  }
}

BaseLayerSwitcher.propTypes = {
  baseLayers: PropTypes.arrayOf(PropTypes.any),
  onSelect: PropTypes.func,
  currentBaseLayerId: PropTypes.string.isRequired,
  classes: PropTypes.objectOf(PropTypes.any).isRequired
};

BaseLayerSwitcher.defaultProps = {
  baseLayers: [],
  onSelect: () => {}
};

export default withStyles(styles)(BaseLayerSwitcher);
