import constants from "./layersConstants";

const layers = {
  accountLayers: {},
  baseLayers: [],
  currentBaseLayerId: "OSM",
  currentLayerId: undefined,
  clusteringLayers: [],
  statusText: "",
  error: ""
};

const layersReducer = (state = layers, action) => {
  const { payload } = action;
  switch (action.type) {
    case constants.GET_LAYERS_REQUEST:
      return Object.assign({}, state, {
        statusText: "Идет загрузка слоев",
        error: ""
      });
    case constants.GET_LAYERS_FAILURE:
      return Object.assign({}, state, {
        error: payload,
        statusText: "Не удалось загрузить слои"
      });
    case constants.GET_LAYERS_SUCCESS:
      return Object.assign({}, state, {
        accountLayers: payload.layers,
        baseLayers: payload.baseLayers,
        statusText: "Слои успещно загружены",
        error: ""
      });
    case constants.UPDATE_LAYER_REQUEST:
      return Object.assign({}, state, {
        statusText: "Идет обновление слоя",
        error: ""
      });
    case constants.UPDATE_LAYER_FAILURE:
      return Object.assign({}, state, {
        error: payload,
        statusText: "Не удалось обновить слой"
      });
    case constants.UPDATE_LAYER_SUCCESS:
      return Object.assign({}, state, {
        accountLayers: payload,
        statusText: "Слой успещно обновлен",
        error: ""
      });
    case constants.UPDATE_LAYERS_SUCCESS:
      return Object.assign({}, state, {
        accountLayers: payload,
        statusText: "Слои успещно обновлены",
        error: ""
      });
    case constants.UPDATE_BASE_LAYER_SUCCESS:
      return Object.assign({}, state, {
        currentBaseLayerId: payload,
        statusText: "Базовый слой успещно обновлен",
        error: ""
      });
    case constants.SET_CURRENT_LAYER:
      return Object.assign({}, state, {
        currentLayerId: payload,
        statusText: "Установлен слой для рекдактирования",
        error: ""
      });
    case constants.SET_CLUSTERING_LAYERS:
      return Object.assign({}, state, {
        clusteringLayers: payload,
        statusText: "Обновлен набор слоев кластризации",
        error: ""
      });
    default:
      return state;
  }
};

export default layersReducer;
