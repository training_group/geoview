import constants from "./layersConstants";
import { layersService } from "../../appServices";

function getLayers(user) {
  const onRequest = () => ({
    type: constants.GET_LAYERS_REQUEST
  });
  const onSuccess = payload => ({
    type: constants.GET_LAYERS_SUCCESS,
    payload
  });

  const onFailure = payload => ({
    type: constants.GET_LAYERS_FAILURE,
    payload
  });

  return async dispatch => {
    dispatch(onRequest());
    let layers = {};
    try {
      layers = await layersService.getLayers(user);
      dispatch(onSuccess(layers));
    } catch (e) {
      const text = `Не удалось загрузить слои: ${e.message}`;
      dispatch(onFailure(text));
    }
    return layers;
  };
}

function updateLayers(layers) {
  const onSuccess = payload => ({
    type: constants.UPDATE_LAYERS_SUCCESS,
    payload
  });

  return async dispatch => {
    dispatch(onSuccess(layers));
    return layers;
  };
}

function updateBaseLayer(baseLayerId) {
  const onSuccess = payload => ({
    type: constants.UPDATE_BASE_LAYER_SUCCESS,
    payload
  });

  return async dispatch => {
    dispatch(onSuccess(baseLayerId));
    return baseLayerId;
  };
}

function setCurrentLayer(layerId) {
  const onSuccess = payload => ({
    type: constants.SET_CURRENT_LAYER,
    payload
  });

  return async dispatch => {
    dispatch(onSuccess(layerId));
    return layerId;
  };
}

function setClusteringLayers(layers) {
  const onSuccess = payload => ({
    type: constants.SET_CLUSTERING_LAYERS,
    payload
  });

  return async dispatch => {
    dispatch(onSuccess(layers));
    return layers;
  };
}

function updateLayer(id, features, layers) {
  const onRequest = () => ({
    type: constants.UPDATE_LAYER_REQUEST
  });

  const onSuccess = payload => ({
    type: constants.UPDATE_LAYER_SUCCESS,
    payload
  });

  const onFailure = payload => ({
    type: constants.UPDATE_LAYER_FAILURE,
    payload
  });

  return async dispatch => {
    dispatch(onRequest());
    let updatedLayers = { ...layers };
    try {
      const layer = await layersService.updateLayer({ id, features });
      updatedLayers = { ...layers, [id]: layer };
      dispatch(onSuccess(updatedLayers));
      return updatedLayers;
    } catch (e) {
      const text = `Не удалось обновить слой: ${e.message}`;
      dispatch(onFailure(text));
      return updatedLayers;
    }
  };
}
function removeFeatures(id, features, layers) {
  const onRequest = () => ({
    type: constants.UPDATE_LAYER_REQUEST
  });

  const onSuccess = payload => ({
    type: constants.UPDATE_LAYER_SUCCESS,
    payload
  });

  const onFailure = payload => ({
    type: constants.UPDATE_LAYER_FAILURE,
    payload
  });

  return async dispatch => {
    dispatch(onRequest());
    let updatedLayers = { ...layers };
    try {
      const layer = await layersService.removeFeatures({ id, features });
      updatedLayers = { ...layers, [id]: layer };
      dispatch(onSuccess(updatedLayers));
      return updatedLayers;
    } catch (e) {
      const text = `Не удалось обновить слой: ${e.message}`;
      dispatch(onFailure(text));
      return updatedLayers;
    }
  };
}

export default {
  getLayers,
  updateLayers,
  updateBaseLayer,
  setCurrentLayer,
  updateLayer,
  removeFeatures,
  setClusteringLayers
};
