import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import layerActions from "./layersActions";
import LayersService from "../../../core/layers/service/layersService";
import mapEditorFunctions from "../../mapEditor/store/mapEditorActions";

function addLayersProps(ComposedComponent) {
  class LayersProps extends Component {
    constructor(props) {
      super(props);
      this.actions = {
        toggleVisibility: this.toggleVisibility.bind(this)
      };
    }

    toggleVisibility(layerId) {
      const { layers, layerActions, mapViewer, mapEditorActions } = this.props;
      const layer = layers[layerId];
      const { visible } = layer;

      const currentVisible = !visible;

      const layersToUpdate = LayersService.changeVisibility(
        layer,
        currentVisible,
        layers
      );
      mapViewer.updateLayers(layersToUpdate);
      layerActions.updateLayers(layersToUpdate);
      mapEditorActions.setTool(undefined);
      layerActions.setCurrentLayer(undefined);
    }

    render() {
      const { children, ...other } = this.props;

      return (
        <ComposedComponent actions={this.actions} {...other}>
          {children}
        </ComposedComponent>
      );
    }
  }

  function mapStateToProps(state) {
    return {
      layers: state.layers.accountLayers,
      mapViewer: state.mapEditor.viewer
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
      layerActions: bindActionCreators(layerActions, dispatch),
      mapEditorActions: bindActionCreators(mapEditorFunctions, dispatch)
    };
  }

  return connect(mapStateToProps, mapDispatchToProps)(LayersProps);
}

export default addLayersProps;
