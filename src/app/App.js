import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import thunk from "redux-thunk";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import appReducers from "./appReducers";
import AppRoutes from "./AppRoutes";

import "./App.css";

const store = createStore(appReducers, applyMiddleware(thunk));
const theme = createMuiTheme({
  typography: {
    useNextVariants: true
  },
  overrides: {
    MuiFormHelperText: {
      root: {
        color: "#f24746"
      }
    }
  }
});

const App = () => (
  <div className="App Scrollbar">
    <Provider store={store}>
      <MuiThemeProvider theme={theme}>
        <Router>
          <AppRoutes />
        </Router>
      </MuiThemeProvider>
    </Provider>
  </div>
);

export default App;
