import React, {
  Component, Children, isValidElement, Fragment,
} from 'react';
import { withStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

const stylesMui = () => ({
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1,
  },
  tabLabelIcon: {
    minHeight: 'auto',
    paddingTop: 0,
  },
  tabRoot: {
    minWidth: '112px',
    height: '48px',
    boxSizing: 'border-box',
    textTransform: 'none',
    fontSize: '14px',
  },
  tabWrapper: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  tabLabelContainer: {
    padding: '0px',
    width: 'auto',
  },
  root: {
    padding: '0 12px',
  },
  iconRoot: {
    fontSize: '18px',
    marginRight: '3px',
  },
  tabTextColorInherit: {
    color: '#FFFFFF',
  },
  tabSelected: {
    color: '#9ACFFF',
  },
  tabsIndicator: {
    backgroundColor: '#9ACFFF',
  },
  tabs: {
    display: 'flex',
    backgroundColor: 'rgba(80, 80, 80, 0.7)',
    boxShadow: '0 2px 2px rgba(0, 0, 0, 0.25)',
    minHeight: '48px',
  },
  content: {
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1,
  },
});


class DomainObjectFormSections extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 1,
    };
    this.getTabsWithContent = this.getTabsWithContent.bind(this);
    this.getTabClasses = this.getTabClasses.bind(this);
    this.getTabsClasses = this.getTabsClasses.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }


  getTabsClasses() {
    const { classes } = this.props;
    return {
      root: classes.root,
      indicator: classes.tabsIndicator,
    };
  }

  getTabClasses() {
    const { classes } = this.props;
    return {
      root: classes.tabRoot,
      wrapper: classes.tabWrapper,
      labelContainer: classes.tabLabelContainer,
      labelIcon: classes.tabLabelIcon,
      textColorInherit: classes.tabTextColorInherit,
      selected: classes.tabSelected,
    };
  }

  getTabsWithContent() {
    const { children } = this.props;
    const { value } = this.state;
    const tabClasses = this.getTabClasses();

    let content;

    const tabs = Children.map(children, (child, index) => {
      if (isValidElement(child)) {
        const { icon, title } = child.type;

        if (index === value) {
          content = child;
        }

        return (
          <Tab
            value={index}
            icon={icon}
            classes={tabClasses}
            label={title}
          />
        );
      }
      return undefined;
    });

    return { tabs, content, value };
  }

  handleChange(event, value) {
    this.setState({ value });
  }

  render() {
    const { classes } = this.props;
    const tabsClasses = this.getTabsClasses();
    const { tabs, content, value } = this.getTabsWithContent();
    return (
      <Fragment>
        <div className={classes.tabs}>
          <Tabs
            classes={tabsClasses}
            value={value}
            onChange={this.handleChange}
          >
            {tabs}
          </Tabs>
        </div>
        <div className={classes.content}>
          { content }
        </div>
      </Fragment>
    );
  }
}

export default withStyles(stylesMui)(DomainObjectFormSections);
