import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import DomainObjectFormTitle from './DomainObjectFormTitle';

const stylesMui = () => ({
  item: {
    backgroundColor: 'rgba(80, 80, 80, 0.7)',
    padding: '0 12px',
    boxSizing: 'border-box',
  },
  divider: {
    backgroundColor: 'rgba(204, 204, 204, 0.5)',
  },
});


const DomainObjectFormSectionsHeader = ({ classes, title, subtitle, children }) => {
  const dividerClasses = {
    root: classes.divider,
  };

  return (
    <div className={classes.item}>
      <DomainObjectFormTitle title={title} subtitle={subtitle}>
        { children }
      </DomainObjectFormTitle>
      <Divider classes={dividerClasses} />
    </div>
  );
};

export default withStyles(stylesMui)(DomainObjectFormSectionsHeader);
