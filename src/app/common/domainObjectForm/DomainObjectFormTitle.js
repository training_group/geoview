import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const styles = () => ({
  primary: {
    color: '#9ACFFF',
    fontSize: '18px',
  },
  secondary: {
    color: 'white',
    fontSize: '11px',
  },
  listItemGutters: {
    paddingLeft: 0,
    paddingRight: 0,
  },
});

const DomainObjectFormTitle = ({
  id, name, classes, title, subtitle, onClick, children, disabled, button, ...rest
}) => {
  const onClickItem = ({ currentTarget: { id, name } }) => {
    if (onClick) {
      onClick(id, name);
    }
  };

  const classesItemText = {
    primary: classes.primary,
    secondary: classes.secondary,
  };

  const ListItemClasses = {
    gutters: classes.listItemGutters,
  };

  return (
    <ListItem
      id={id}
      classes={ListItemClasses}
      button={button}
      name={name}
      onClick={onClickItem}
      disabled={disabled}
    >
      <ListItemText classes={classesItemText} primary={title} secondary={subtitle} {...rest} />
      { children }
    </ListItem>
  );
};

export default withStyles(styles)(DomainObjectFormTitle);
