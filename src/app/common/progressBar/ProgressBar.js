import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';


const stylesMui = () => ({
  wrapper: {
    display: 'flex',
    alignItems: 'center',
    margin: '10px',
  },
  message: {
    textAlign: 'center',
    color: '#CCCCCC',
    fontSize: '12px',
    margin: '8px',
  },
  circularColorPrimary: {
    color: '#CCCCCC',
  },
});


const ProgressBar = ({ classes, message }) => {
  const classesCircular = {
    colorPrimary: classes.circularColorPrimary,
  };
  return (
    <div className={classes.wrapper}>
      <div>
        <CircularProgress size={20} thickness={5} classes={classesCircular} />
      </div>
      <div className={classes.message}>{ message }</div>
    </div>
  );
};


export default withStyles(stylesMui)(ProgressBar);
