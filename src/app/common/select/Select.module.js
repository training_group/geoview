const stylesMui = () => ({
  inputRoot: {
    border: "1px solid #6f6c6c",
    color: "#CCCCCC",
    padding: "8px 6px",
    boxSizing: "border-box",
    minHeight: "40px",
    fontSize: "12px"
  },
  inputMultiline: {
    resize: "none",
    height: "80px !important",
    marginBottom: "20px"
  },
  formControl: {
    marginTop: "0 !important",
    padding: "0 4px 0 8px"
  },
  formControlRoot: {
    minHeight: "86px",
    boxSizing: "border-box",
    marginBottom: "4px"
  },
  root: {
    color: "#ffffff !important",
    position: "relative",
    transform: "none",
    fontSize: "12px",
    fontWeight: "bold",
    lineHeight: "26px",
    "&$focused": {
      color: "#9ACFFF"
    }
  },
  icon: {
    color: "#CCCCCC"
  },
  selectMenu: {
    backgroundColor: "#3D3E40"
  },
  menuItem: {
    color: "#CCCCCC"
  }
});

export default stylesMui;
