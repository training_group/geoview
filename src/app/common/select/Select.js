import React, { Children, isValidElement } from "react";
import { withStyles } from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import SelectBase from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Input from "@material-ui/core/Input";
import stylesMui from "./Select.module";

const Select = ({
  className,
  name,
  classes,
  id,
  label,
  fullWidth,
  children,
  value,
  ...props
}) => {
  const inputClasses = {
    root: classes.inputRoot,
    formControl: classes.formControl
  };

  const labelClasses = {
    root: classes.root,
    focused: classes.focused
  };

  const formControlClasses = {
    root: classes.formControlRoot
  };

  const selectClasses = {
    icon: classes.icon
  };

  const menuClasses = {
    paper: classes.selectMenu
  };

  const menuItem = {
    root: classes.menuItem
  };

  const currentValue = value || "#";
  return (
    <FormControl
      fullWidth={fullWidth}
      classes={formControlClasses}
      className={className}
    >
      <InputLabel
        className={Object.values(labelClasses).join(" ")}
        disableAnimation
        htmlFor={id}
      >
        {label}
      </InputLabel>
      <SelectBase
        disableUnderline
        input={
          <Input
            name={name}
            classes={inputClasses}
            {...props}
            disableUnderline
            autoComplete="off"
          />
        }
        value={currentValue}
        classes={selectClasses}
        MenuProps={{ classes: menuClasses }}
      >
        {Children.map(children, child => {
          if (!isValidElement(child)) {
            return undefined;
          }
          const valueItem = child.props.value || "#";
          return (
            <MenuItem classes={menuItem} value={valueItem}>
              {child}
            </MenuItem>
          );
        })}
      </SelectBase>
    </FormControl>
  );
};

export default withStyles(stylesMui)(Select);
