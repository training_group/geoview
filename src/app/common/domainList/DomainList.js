import React from 'react';
import PropTypes from 'prop-types';
import List from '@material-ui/core/List';
import styles from './List.module.css';

const DomainList = ({ children }) => (
  <List className={styles.rootList} disablePadding>
    { children }
  </List>
);

DomainList.propTypes = {
  //children: PropTypes.arrayOf(PropTypes.node),
};

DomainList.defaultProps = {
  //children: undefined,
};

export default DomainList;
