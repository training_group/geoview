import React from 'react';
import PropTypes from 'prop-types';
import FolderOpen from '@material-ui/icons/FolderOpen';
import FolderClose from '@material-ui/icons/Folder';
import styles from './List.module.css';

const classes = {
  root: styles.expandIcon,
};

const ExpandIcon = ({ open, ...other }) => (
  open ? <FolderOpen classes={classes} {...other} /> : <FolderClose classes={classes} {...other} />
);

ExpandIcon.propTypes = {
  open: PropTypes.bool.isRequired,
};

export default ExpandIcon;
