import React, { Component } from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import ExpandIcon from "./ExpandIcon";

import styles from "./List.module.css";

class DomainListItem extends Component {
  static isHasNested(nested) {
    return nested && nested.length !== 0;
  }

  constructor(props) {
    super(props);
    this.state = {
      open: true
    };
    this.addNested = this.addNested.bind(this);
    this.addExpand = this.addExpand.bind(this);
    this.onCollapse = this.onCollapse.bind(this);
  }

  onCollapse() {
    this.setState(({ open }) => ({ open: !open }));
  }

  addExpand() {
    const { nested, itemIcon, iconClasses } = this.props;
    const { open } = this.state;

    if (itemIcon) {
      const Icon = itemIcon;
      return <Icon classes={iconClasses} />;
    }

    const hasNested = DomainListItem.isHasNested(nested);

    return hasNested ? (
      <ExpandIcon open={open} onClick={this.onCollapse} />
    ) : (
      undefined
    );
  }

  addNested() {
    const { open } = this.state;
    const { nested } = this.props;

    const hasNested = DomainListItem.isHasNested(nested);

    if (!hasNested) {
      return undefined;
    }

    return (
      <Collapse key={2} in={open} timeout="auto" unmountOnExit>
        <List disablePadding>{nested}</List>
      </Collapse>
    );
  }

  render() {
    const { children, title, inset, toolbar } = this.props;

    const classesItemText = {
      root: !inset ? styles.root_itemText : styles.inset
    };

    return (
      <div className={styles.wrapper}>
        <ListItem className={styles.root} disableGutters key={1}>
          {this.addExpand()}
          <ListItemText
            classes={classesItemText}
            inset={inset}
            disableTypography
            primary={title}
            onClick={this.onCollapse}
          />
          {children}
        </ListItem>
        {toolbar}
        {this.addNested()}
      </div>
    );
  }
}

export default DomainListItem;
