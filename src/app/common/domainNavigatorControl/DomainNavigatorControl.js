import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Arrow from '@material-ui/icons/ArrowRightAlt';
import ButtonBase from '@material-ui/core/ButtonBase';

import styles from './DomainNavigatorControl.module.css';


const DomainNavigatorControl = ({
  title,
  onNext,
  onPrev,
}) => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.toolbar}>
        <ButtonBase onClick={onPrev}>
          <Arrow className={styles.arrowLeft} />
        </ButtonBase>
        <div className={styles.text}>
          {title}
        </div>
        <ButtonBase onClick={onNext}>
          <Arrow />
        </ButtonBase>
      </div>
    </div>
  );
};

export default DomainNavigatorControl;
