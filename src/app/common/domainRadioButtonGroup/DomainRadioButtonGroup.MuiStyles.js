const muiStyles = theme => ({
  formControl: {
    margin: 0,
    display: "flex",
    alignItems: "flex-start",
    flexDirection: "column"
  },
  formGroup: {
    margin: 0,
    display: "flex",
    alignItems: "flex-start",
    flexDirection: "row"
  },
  radioGroup: {
    flexDirection: "row",
    alignItems: "flex-start"
  },
  radio: {
    padding: 0
  },
  label: {
    margin: 0
  },
  formLabel: {
    display: "inline-flex",
    width: "auto",
    fontSize: 10,
    color: "white",
    backgroundColor: "rgba(0,0,0,0.3)",
    padding: 2,
    marginTop: 4,
    marginRight: 4,
    borderRadius: 2,
    "&:hover": {
      color: "rgb(76, 148, 214)"
    }
  },
  formLabelFocused: {
    color: "white"
  },
  active: {
    compose: "default",
    fill: "#9ACFFF",
    fontSize: 20,
    marginRight: 2,
    "&:hover": {
      fill: "rgb(76, 148, 214)"
    }
  },
  icons: {
    compose: "default",
    fill: "#CCCCCC",
    fontSize: 20,
    marginRight: 2,
    "&:hover": {
      fill: "rgb(76, 148, 214)"
    }
  }
});
export default muiStyles;
