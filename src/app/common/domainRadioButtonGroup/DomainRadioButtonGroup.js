import React, { Component } from "react";
import PropTypes from "prop-types";
import FormControl from "@material-ui/core/FormControl";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import { withStyles } from "@material-ui/core/styles";

import muiStyles from "./DomainRadioButtonGroup.MuiStyles";

const DomainRadioButtonGroup = ({
  value,
  values,
  onChange,
  additionalActions,
  classes,
  ...custom
}) => {
  return (
    <FormControl className={classes.formControl}>
      <RadioGroup
        value={value}
        className={classes.radioGroup}
        onChange={event => onChange(event.target.value)}
      >
        {values.map(valueItem => {
          const Icon = valueItem.icon;
          return (
            <FormControlLabel
              key={valueItem.value}
              value={valueItem.value}
              classes={{ root: classes.label }}
              control={
                <Radio
                  checkedIcon={<Icon className={classes.active} />}
                  icon={<Icon className={classes.icons} />}
                  classes={{
                    root: classes.radio
                  }}
                />
              }
            />
          );
        })}
      </RadioGroup>
      <FormGroup className={classes.formGroup}>
        {additionalActions.map((action, idx) => {
          return (
            <span
              id={action.value}
              key={action.value}
              onClick={action.click}
              className={classes.formLabel}
            >
              {action.name}
            </span>
          );
        })}
      </FormGroup>
    </FormControl>
  );
};

DomainRadioButtonGroup.propTypes = {
  value: PropTypes.string,
  values: PropTypes.arrayOf(PropTypes.any),
  onChange: PropTypes.func,
  additionalActions: PropTypes.arrayOf(PropTypes.objectOf(PropTypes.any)),
  classes: PropTypes.objectOf(PropTypes.any).isRequired
};

DomainRadioButtonGroup.defaultProps = {
  additionalActions: []
};

export default withStyles(muiStyles)(DomainRadioButtonGroup);
