import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";

const DragIcon = ({ className, ...other }) => (
  <SvgIcon viewBox="0 0 24 24" className={className} {...other}>
    <g
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      fillRule="evenodd"
      transform="translate(-3 -3)"
    >
      <mask id="dragIcon-2" fill="#fff">
        <path
          xmlns="http://www.w3.org/2000/svg"
          id="dragIcon-1"
          d="M21 14v-4l6 5-6 5v-4h-5v5h4l-5 6-5-6h4v-5H9v4l-6-5 6-5v4h5V9h-4l5-6 5 6h-4v5h5z"
        />
      </mask>
      <g className={className}>
        <path
          xmlns="http://www.w3.org/2000/svg"
          id="dragIcon-3"
          d="M21 14v-4l6 5-6 5v-4h-5v5h4l-5 6-5-6h4v-5H9v4l-6-5 6-5v4h5V9h-4l5-6 5 6h-4v5h5z"
        />
      </g>
      <g className={className} mask="url(#dragIcon-2)">
        <path d="M0 0h30v30H0z" />
      </g>
    </g>
  </SvgIcon>
);

export default DragIcon;
