import React from 'react';
import SvgIcon from '@material-ui/core/SvgIcon';

const BulletIcon = props => (
  <SvgIcon viewBox="0 0 8 8" {...props}>
    <circle cx="4" cy="4" r="4" />
  </SvgIcon>
);

export default BulletIcon;
