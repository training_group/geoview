import React from 'react';

const ExitIcon = () => (<img alt='exit' src='/exit.svg' />);

export default ExitIcon;
