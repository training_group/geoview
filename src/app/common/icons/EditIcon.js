import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";

const EditIcon = ({ className, ...other }) => (
  <SvgIcon viewBox="0 0 24 24" className={className} {...other}>
    <g fill="none" fillRule="evenodd" transform="translate(-3 -3)">
      <mask id="EditIcon-2" fill="#fff">
        <path
          id="EditIcon-1"
          d="M13.5 11a3.5 3.5 0 110-7 3.5 3.5 0 010 7zm0-2a1.5 1.5 0 100-3 1.5 1.5 0 000 3zM12 7.53a1.5 1.5 0 00.98 1.378L3 15v-2l9-5.47zm2.217-1.348L19.453 3h3.206l-7.67 4.682a1.5 1.5 0 00-.772-1.5zm9.226 13.103h-3.315l1.745 4.25a.58.58 0 01-.295.75l-1.537.67a.553.553 0 01-.729-.304l-1.658-4.036-2.708 2.786c-.36.371-.946.085-.946-.402V9.572c0-.513.623-.763.946-.402l8.888 9.142c.359.35.094.973-.39.973z"
        />
      </mask>
      <g className={className} fillRule="nonzero">
        <path
          id="EditIcon-3"
          d="M13.5 11a3.5 3.5 0 110-7 3.5 3.5 0 010 7zm0-2a1.5 1.5 0 100-3 1.5 1.5 0 000 3zM12 7.53a1.5 1.5 0 00.98 1.378L3 15v-2l9-5.47zm2.217-1.348L19.453 3h3.206l-7.67 4.682a1.5 1.5 0 00-.772-1.5zm9.226 13.103h-3.315l1.745 4.25a.58.58 0 01-.295.75l-1.537.67a.553.553 0 01-.729-.304l-1.658-4.036-2.708 2.786c-.36.371-.946.085-.946-.402V9.572c0-.513.623-.763.946-.402l8.888 9.142c.359.35.094.973-.39.973z"
        />
      </g>
      <g className={className} mask="url(#EditIcon-2)">
        <path d="M0 0h30v30H0z" />
      </g>
    </g>
  </SvgIcon>
);

export default EditIcon;
