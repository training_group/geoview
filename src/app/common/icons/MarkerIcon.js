import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";

const MarkerIcon = props => (
  <SvgIcon viewBox="0 0 24 24" {...props}>
    <path
      d="M15.5 24.878c-.21 0-.326-.031-.84-.643C10.22 19.412 8 15.501 8 12.505A7.502 7.502 0 0115.5 5c4.142 0 7.5 3.36 7.5 7.504 0 4.496-4.712 9.423-6.666 11.74-.512.606-.625.635-.834.634zm0-9.345c1.775 0 3.214-1.415 3.214-3.16s-1.439-3.16-3.214-3.16-3.214 1.415-3.214 3.16 1.439 3.16 3.214 3.16z"
      id="MarkerIcon-1"
      transform="translate(-3 -3)"
      fillRule="nonzero"
    />
  </SvgIcon>
);

export default MarkerIcon;
