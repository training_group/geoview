import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";

const IntersectionIcon = ({ className, ...other }) => (
  <SvgIcon viewBox="0 0 512 512" className={className} {...other}>
    <path
      d="M497,182H349.848C353.787,82.402,273.874,0,175,0C78.505,0,0,78.505,0,175c0,98.84,82.367,178.789,182,174.848V497
			c0,8.284,6.716,15,15,15h300c8.284,0,15-6.716,15-15V197C512,188.716,505.284,182,497,182z M182,197v122.834
			C98.92,323.767,30,257.311,30,175C30,95.047,95.047,30,175,30c82.307,0,148.767,68.916,144.834,152H197
			C188.716,182,182,188.716,182,197z M482,482H212V346.076C278.741,331.652,331.596,279.001,346.076,212H482V482z"
    />
  </SvgIcon>
);

export default IntersectionIcon;
