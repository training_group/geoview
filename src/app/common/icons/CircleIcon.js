import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";

const CircleIcon = ({ className, ...other }) => (
  <SvgIcon viewBox="0 0 24 24" className={className} {...other}>
    <g fill="none" fillRule="evenodd" transform="translate(-3 -3)">
      <mask id="CircleIcon-2" fill="#fff">
        <path
          d="M18.29 6.786a3.5 3.5 0 014.924 4.924A9.468 9.468 0 0124 15.5 9.5 9.5 0 1114.5 6c1.347 0 2.629.28 3.79.786zm-1.14 1.696a7.5 7.5 0 104.368 4.368 3.5 3.5 0 01-4.368-4.368zM14.5 17a1.5 1.5 0 110-3 1.5 1.5 0 010 3zm6-6a1.5 1.5 0 100-3 1.5 1.5 0 000 3z"
          id="CircleIcon-1"
        />
      </mask>
      <g className={className} fillRule="nonzero">
        <path
          d="M18.29 6.786a3.5 3.5 0 014.924 4.924A9.468 9.468 0 0124 15.5 9.5 9.5 0 1114.5 6c1.347 0 2.629.28 3.79.786zm-1.14 1.696a7.5 7.5 0 104.368 4.368 3.5 3.5 0 01-4.368-4.368zM14.5 17a1.5 1.5 0 110-3 1.5 1.5 0 010 3zm6-6a1.5 1.5 0 100-3 1.5 1.5 0 000 3z"
          id="CircleIcon-3"
        />
      </g>
      <g className={className} mask="url(#CircleIcon-2)">
        <path d="M0 0h30v30H0z" />
      </g>
    </g>
  </SvgIcon>
);

export default CircleIcon;
