import React from 'react';

const OutSourceIcon = () => (<img alt='out source' src='/outSources.svg' />);

export default OutSourceIcon;