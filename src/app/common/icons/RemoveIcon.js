import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";

const RemoveIcon = ({ className, ...other }) => (
  <SvgIcon viewBox="0 0 24 24" className={className} {...other}>
    <g fillRule="nonzero" transform="translate(-3 -3)">
      <path
        d="M17.787 18.481l-6.139-5.131-5.243 6.032 4.149 3.606h3.315l3.918-4.507zm-1.28 4.507H26v2H9.807l-4.714-4.097a2 2 0 01-.198-2.822L16.048 5.24a2 2 0 012.822-.197l6.037 5.249a2 2 0 01.198 2.821l-8.598 9.876z"
        id="RemoveIcon-1"
      />
    </g>
  </SvgIcon>
);

export default RemoveIcon;
