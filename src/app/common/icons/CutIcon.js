import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";

const CutIcon = ({ className, ...other }) => (
  <SvgIcon viewBox="0 0 24 24" className={className} {...other}>
    <g fillRule="nonzero" transform="rotate(-32 9.362 19.394)">
      <path
        d="M12.97 13.494l8.062-7.952 2.433.135-6.418 8.834 10.519 2.622-1.777 1.668-11.204-.902-.936 1.289a3.5 3.5 0 11-2.215-.354l1.247-1.716-.157-.743-.573-1.074-2.058-.513a3.5 3.5 0 111.469-1.695l1.607.4zm-5.212-.269a1.5 1.5 0 10.726-2.91 1.5 1.5 0 00-.726 2.91zm3.045 8.178a1.5 1.5 0 102.427 1.763 1.5 1.5 0 00-2.427-1.763z"
        id="a"
      />
    </g>
  </SvgIcon>
);

export default CutIcon;
