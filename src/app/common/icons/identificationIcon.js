import React from 'react';
import SvgIcon from '@material-ui/core/SvgIcon';

const IdentificationIcon = props => (
  <SvgIcon viewBox="0 0 24 24" {...props}>
    <g>
      <polygon points="21,12.864 7,7 12.857,21 13.619,21 15.124,17.116 18.661,20.653 20.649,18.664 17.112,15.127 21,13.627  " />
      <circle cx="4.127" cy="4.131" r="2" />
    </g>
  </SvgIcon>
);

export default IdentificationIcon;
