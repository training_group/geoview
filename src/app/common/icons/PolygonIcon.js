import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";

const PolygonIcon = ({ className, ...other }) => (
  <SvgIcon viewBox="0 0 24 24" className={className} {...other}>
    <g fill="none" fillRule="evenodd" transform="translate(-3 -3)">
      <mask id="PolygonIcon-2" fill="#fff">
        <path
          id="PolygonIcon-1"
          d="M19.42 9.165a3.5 3.5 0 113.58 1.8v8.07A3.5 3.5 0 1119.035 23h-8.07a3.5 3.5 0 11-1.8-3.58L19.421 9.166zm1.415 1.414L10.579 20.835c.03.054.058.11.084.165h8.674A3.514 3.514 0 0121 19.337v-8.674a3.488 3.488 0 01-.165-.084zM22.5 9a1.5 1.5 0 100-3 1.5 1.5 0 000 3zm0 15a1.5 1.5 0 100-3 1.5 1.5 0 000 3zm-15 0a1.5 1.5 0 100-3 1.5 1.5 0 000 3z"
        />
      </mask>
      <g className={className} fillRule="nonzero">
        <path
          id="PolygonIcon-3"
          d="M19.42 9.165a3.5 3.5 0 113.58 1.8v8.07A3.5 3.5 0 1119.035 23h-8.07a3.5 3.5 0 11-1.8-3.58L19.421 9.166zm1.415 1.414L10.579 20.835c.03.054.058.11.084.165h8.674A3.514 3.514 0 0121 19.337v-8.674a3.488 3.488 0 01-.165-.084zM22.5 9a1.5 1.5 0 100-3 1.5 1.5 0 000 3zm0 15a1.5 1.5 0 100-3 1.5 1.5 0 000 3zm-15 0a1.5 1.5 0 100-3 1.5 1.5 0 000 3z"
        />
      </g>
      <g className={className} mask="url(#PolygonIcon-2)">
        <path d="M0 0h30v30H0z" />
      </g>
    </g>
  </SvgIcon>
);

export default PolygonIcon;
