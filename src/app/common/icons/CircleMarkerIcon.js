import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";

const CircleMarkerIcon = ({ className, ...other }) => (
  <SvgIcon viewBox="0 0 100 100" className={className} {...other} fill="none">
    <circle cx="50" cy="50" r="35" />
    <circle cx="50" cy="50" r="3" className={className} />
  </SvgIcon>
);

export default CircleMarkerIcon;
