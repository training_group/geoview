import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";

const RectangleIcon = ({ className, ...other }) => (
  <SvgIcon viewBox="0 0 24 24" className={className} {...other}>
    <g fill="none" fillRule="evenodd" transform="translate(-3 -3)">
      <mask id="RectangleIcon-2" fill="#fff">
        <path
          id="RectangleIcon-1"
          d="M23 10.965v8.07A3.5 3.5 0 1119.035 23h-8.07A3.5 3.5 0 117 19.035v-8.07A3.5 3.5 0 1110.965 7h8.07A3.5 3.5 0 1123 10.965zm-2-.302A3.514 3.514 0 0119.337 9h-8.674A3.514 3.514 0 019 10.663v8.674A3.514 3.514 0 0110.663 21h8.674A3.514 3.514 0 0121 19.337v-8.674zM7.5 9a1.5 1.5 0 100-3 1.5 1.5 0 000 3zm15 0a1.5 1.5 0 100-3 1.5 1.5 0 000 3zm0 15a1.5 1.5 0 100-3 1.5 1.5 0 000 3zm-15 0a1.5 1.5 0 100-3 1.5 1.5 0 000 3z"
        />
      </mask>
      <path
        className={className}
        fillRule="nonzero"
        id="RectangleIcon-3"
        d="M23 10.965v8.07A3.5 3.5 0 1119.035 23h-8.07A3.5 3.5 0 117 19.035v-8.07A3.5 3.5 0 1110.965 7h8.07A3.5 3.5 0 1123 10.965zm-2-.302A3.514 3.514 0 0119.337 9h-8.674A3.514 3.514 0 019 10.663v8.674A3.514 3.514 0 0110.663 21h8.674A3.514 3.514 0 0121 19.337v-8.674zM7.5 9a1.5 1.5 0 100-3 1.5 1.5 0 000 3zm15 0a1.5 1.5 0 100-3 1.5 1.5 0 000 3zm0 15a1.5 1.5 0 100-3 1.5 1.5 0 000 3zm-15 0a1.5 1.5 0 100-3 1.5 1.5 0 000 3z"
      />
      <g className={className} mask="url(#RectangleIcon-2)">
        <path d="M0 0h30v30H0z" />
      </g>
    </g>
  </SvgIcon>
);

export default RectangleIcon;
