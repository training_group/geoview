import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";

const LineIcon = ({ className, ...other }) => (
  <SvgIcon viewBox="0 0 24 24" className={className} {...other}>
    <g fill="none" fillRule="evenodd" transform="translate(-3 -3)">
      <mask id="LineIcon-2" fill="#fff">
        <path
          id="LineIcon-1"
          d="M9.165 19.42l9.256-9.255a3.5 3.5 0 111.414 1.414l-9.256 9.256a3.5 3.5 0 11-1.414-1.414zM21.5 10a1.5 1.5 0 100-3 1.5 1.5 0 000 3zm-14 14a1.5 1.5 0 100-3 1.5 1.5 0 000 3z"
        />
      </mask>
      <path
        id="LineIcon-3"
        d="M9.165 19.42l9.256-9.255a3.5 3.5 0 111.414 1.414l-9.256 9.256a3.5 3.5 0 11-1.414-1.414zM21.5 10a1.5 1.5 0 100-3 1.5 1.5 0 000 3zm-14 14a1.5 1.5 0 100-3 1.5 1.5 0 000 3z"
      />
      <g className={className} mask="url(#LineIcon-2)">
        <path d="M0 0h30v30H0z" />
      </g>
    </g>
  </SvgIcon>
);

export default LineIcon;
