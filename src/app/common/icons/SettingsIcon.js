import React from 'react';

const SettingsIcon = () => (<img alt='settings' src='/settings.svg' />);

export default SettingsIcon;