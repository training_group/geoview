const styles = {
  form: {
    display: "flex",
    flexDirection: "column",
    flexGrow: 1,
    boxSizing: "border-box",
    width: "100%"
  },
  verticalDivider: {
    backgroundColor: "#8a8a8a4f",
    width: "1px",
    height: "26px"
  },
  controls: {
    display: "flex",
    justifyContent: "space-around",
    boxSizing: "border-box",
    alignItems: "center",
    minHeight: "40px",
    width: "100%",
    marginBottom: "14px",
    height: "40px",
    boxShadow: "0 0 4px rgba(0, 0, 0, 0.25)",
    backgroundColor: "rgba(80, 80, 80, 0.7)"
  },
  body: {
    display: "flex",
    flexDirection: "column",
    flexGrow: 1,
    boxSizing: "border-box",
    overflowY: "auto",
    marginBottom: "14px",
    marginTop: "14px",
    padding: "12px"
    // alignItems: 'center',
  },
  root: {
    flexGrow: 1,
    textTransform: "none",
    borderRadius: 0,
    color: "#9d9d9e",
    "&$disabled": {
      color: "#9d9d9e"
    }
  },
  disabled: {}
};

const stylesMultiSelect = {
  checkBoxRoot: {
    padding: 0,
    color: "#CCCCCC !important"
  },
  iconRoot: {
    width: "16px",
    height: "16px"
  },
  formControlRoot: {
    minHeight: "86px",
    boxSizing: "border-box",
    marginBottom: "4px"
  },
  root: {
    color: "#ffffff",
    position: "relative",
    transform: "none",
    fontSize: "12px",
    fontWeight: "bold",
    lineHeight: "26px",
    "&$focused": {
      color: "#9ACFFF"
    }
  },
  focused: {}
};
export { styles, stylesMultiSelect };
