import React from "react";
import { Field } from "react-final-form";
import Select from "../select/Select";

const DomainFormFieldSelect = ({
  name,
  alias,
  isEditable,
  type,
  required,
  getValidation,
  options,
  ...other
}) => {
  return (
    <Field id={name} name={name} disabled={!isEditable} label={alias} fullWidth>
      {props => (
        <Select
          name={props.input.name}
          value={props.input.value}
          onChange={props.input.onChange}
          label={alias}
        >
          <div value="">{"Не определено"}</div>
          {options.map(option => {
            const { id, title } = option;
            return (
              <div key={id} id={id} value={id}>
                {title}
              </div>
            );
          })}
        </Select>
      )}
    </Field>
  );
};

export default DomainFormFieldSelect;
