import React from "react";
import { Form } from "react-final-form";
import { Button, Divider, withStyles } from "@material-ui/core";
import { styles } from "./DomainForm.module";

const DomainForm = ({ classes, initialValues, onSave, validate, children }) => {
  const verticalDivider = {
    root: classes.verticalDivider
  };

  const button = {
    root: classes.root,
    disabled: classes.disabled
  };

  const onSubmit = values => {
    if (onSave) {
      onSave(values);
    }
  };

  return (
    <Form
      onSubmit={onSubmit}
      initialValues={initialValues}
      validate={validate}
      render={({ handleSubmit, reset, submitting, pristine, values, form }) => {
        const cancel = () => {
          form.reset(initialValues);
        };
        return (
          <form className={classes.form} onSubmit={handleSubmit} noValidate>
            <div className={classes.body}>{children}</div>
            <div className={classes.controls}>
              <Button
                type="button"
                onClick={cancel}
                disabled={submitting || pristine}
                classes={button}
              >
                Отменить
              </Button>
              <Divider classes={verticalDivider} />
              <Button
                type="submit"
                disabled={submitting || pristine}
                classes={button}
              >
                Сохранить
              </Button>
            </div>
          </form>
        );
      }}
    />
  );
};

export default withStyles(styles)(DomainForm);
