import React, { createContext } from "react";
import { Field } from "react-final-form";
import { withStyles } from "@material-ui/core";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import CheckBoxOutlineBlankIcon from "@material-ui/icons/CheckBoxOutlineBlank";
import CheckBoxIcon from "@material-ui/icons/CheckBox";
import Checkbox from "@material-ui/core/Checkbox";
import { DomainList, DomainListItem } from "../domainList";
import { stylesMultiSelect } from "./DomainForm.module";

const FieldPrefixContext = createContext();

const FieldPrefix = ({ prefix, children }) => (
  <FieldPrefixContext.Provider value={prefix}>
    {children}
  </FieldPrefixContext.Provider>
);
const PrefixedField = ({ name, ...props }) => (
  <FieldPrefixContext.Consumer>
    {prefix => <Field name={`${prefix}.${name}`} {...props} />}
  </FieldPrefixContext.Consumer>
);

const DomainFormMultiSelect = ({ name, alias, items, classes, className }) => {
  const labelClasses = {
    root: classes.root,
    focused: classes.focused
  };

  const formControlClasses = {
    root: classes.formControlRoot
  };

  const createList = items => {
    return items.map(item => {
      const { name: id, alias } = item;

      const checkBoxStyle = {
        root: classes.checkBoxRoot
      };
      const iconStyle = {
        root: classes.iconRoot
      };

      return (
        <DomainListItem key={id} title={alias}>
          <PrefixedField name={id} type="checkbox">
            {props => (
              <Checkbox
                name={props.input.id}
                classes={checkBoxStyle}
                icon={
                  <CheckBoxOutlineBlankIcon
                    classes={iconStyle}
                    fontSize="small"
                  />
                }
                checkedIcon={
                  <CheckBoxIcon classes={iconStyle} fontSize="small" />
                }
                checked={props.input.checked}
                onChange={props.input.onChange}
              />
            )}
          </PrefixedField>
        </DomainListItem>
      );
    });
  };

  return (
    <FieldPrefix prefix={name} label={alias}>
      <FormControl fullWidth classes={formControlClasses} className={className}>
        <InputLabel classes={labelClasses} disableAnimation htmlFor={name}>
          {alias}
        </InputLabel>
        <DomainList>{createList(items)}</DomainList>
      </FormControl>
    </FieldPrefix>
  );
};

export default withStyles(stylesMultiSelect)(DomainFormMultiSelect);
