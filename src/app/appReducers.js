import { combineReducers } from "redux";
import account from "./account/store/accountReducer";
import layers from "./layers/store/layersReducer";
import mapEditor from "./mapEditor/store/mapEditorReducer";
import identification from "./identification/store/identificationReducer";

const appReducers = combineReducers({
  layers,
  account,
  mapEditor,
  identification
});

export default appReducers;
