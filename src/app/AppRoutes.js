import React, { Component } from "react";
import { Route } from "react-router-dom";
import Workspace from "./workspace/Workspace";

class AppRoutes extends Component {
  render() {
    return [<Route key="1" path="/" component={Workspace} />];
  }
}

export default AppRoutes;
