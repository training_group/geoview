class IdentificationItem {
  constructor({ id, title, subTitle, identifiedObject }) {
    this.id = id;
    this.title = title;
    this.subTitle = subTitle;
    this.identifiedObject = identifiedObject;
  }
}

export default IdentificationItem;
