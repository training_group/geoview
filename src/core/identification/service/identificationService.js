import IdentificationItem from "../model/identificationItem";

class IdentificationService {
  constructor(layersObjectsService) {
    this.layersObjectsService = layersObjectsService;
  }

  async identify(geometry, layers) {
    const currentVisibleLayers = [];
    Object.keys(layers).forEach(layerId => {
      const layer = layers[layerId];
      if (layer.visible && !layer.isGroup) {
        currentVisibleLayers.push(layer);
      }
    });
    const layersObjects = await this.layersObjectsService.getLayerObjects(
      geometry,
      currentVisibleLayers
    );
    return IdentificationService.createIdentificationItems(
      layersObjects,
      layers
    );
  }

  static createIdentificationItems(layersObjects, layers) {
    const identificationItems = [];

    if (layersObjects && layersObjects.length) {
      layersObjects.forEach((layersObject, i) => {
        const id = `${i + 1}`;
        const identifiedObject = layersObject;
        const { title, layerId } = layersObject;
        const subTitle = layers[layerId].title;
        const identificationItem = new IdentificationItem({
          id,
          title,
          subTitle,
          identifiedObject
        });
        identificationItems.push(identificationItem);
      });
    }
    return identificationItems;
  }
}

export default IdentificationService;
