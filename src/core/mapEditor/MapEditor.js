class MapEditor {
  constructor({ api, service }) {
    this.api = api;
    this.service = service;
    this.editor = undefined;
    this.currentTool = undefined;
  }

  async init(id) {
    this.editor = await this.api.init(id);
    return this;
  }

  setCurrentConfiguration(config) {
    if (!config) {
      return;
    }

    const { currentBaseLayerId, layers } = config;
    this.setBaseLayer(currentBaseLayerId);

    if (layers) {
      this.updateLayers(layers);
    }
  }

  addBaseLayers(baseLayers) {
    this.editor.addBaseLayers(baseLayers);
  }

  setBaseLayer(baseLayerId) {
    this.editor.setCurrentBaseMap(baseLayerId);
  }

  addLayers(layers) {
    if (!layers) return;
    Object.keys(layers).forEach(layerId => {
      const layer = layers[layerId];
      this.editor.addLayer(layer);
    });
  }

  updateLayers(layers) {
    const layersList = [];
    Object.keys(layers).forEach(layerId => {
      const layer = layers[layerId];
      if (!layer.isGroup) {
        layersList.push(layer);
      }
    });

    this.editor.updateLayers(layersList);
  }

  zoomTo(type, geometryData) {
    this.editor.zoomTo(type, geometryData);
  }

  setIdentification(identify) {
    const identification = () => geomerty => {
      if (identify) {
        identify(geomerty);
      }
    };
    const callback = identification(identify);
    this.editor.setIdentification(callback);
  }

  stopIdentification() {
    this.editor.stopIdentification();
  }

  draw(layerId, toolId, callback) {
    this.editor.draw(layerId, toolId, callback);
  }

  drag(layerId) {
    this.editor.drag(layerId);
  }

  modify(layerId) {
    this.editor.modify(layerId);
  }

  remove(layerId) {
    this.editor.remove(layerId);
  }

  intersection(layerId, toolId, callback) {
    this.editor.intersection(layerId, toolId, callback);
  }

  cancelTool() {
    this.editor.cancelTool();
  }

  updateLayer(layer) {
    this.editor.updateLayer(layer);
  }

  getFeaturesByLayerId(layerId) {
    return this.editor.getFeaturesByLayerId(layerId);
  }

  removeFeatures() {
    return this.editor.removeFeatures();
  }

  addCluster(layerId, type, attributes, layers) {
    this.editor.addCluster(layerId, type, attributes, layers);
  }

  removeCluster(layerId, layers) {
    this.editor.removeCluster(layerId, layers);
  }
}

export default MapEditor;
