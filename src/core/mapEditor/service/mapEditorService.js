
class MapEditorService {
  constructor(layersObjects) {
    this.layersObjects = layersObjects;
  }


  async identify(geometry, layers) {
    const results = await this.layersObjects.intersectsBy(geometry, layers, 10);
    return results;
  }
}


export default MapEditorService;
