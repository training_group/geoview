import Account from '../model/account';

class AccountService {
  constructor(accountApi) {
    this.accountApi = accountApi;
  }

  async signUp(accountData) {
    // TODO  сделать валидацию входящих данных при исключении выдавать ошибку
    const { username, email, authenticated } = await this.accountApi.signUp(accountData);
    const account = new Account(username, email, authenticated);
    return account;
  }

  async signIn(accountData) {

    // TODO  сделать валидацию входящих данных при исключении выдавать ошибку
    if (accountData.username === 'admin' && accountData.password === 'admin') {
      const account = new Account(accountData.username, 'email@mail.com', true);
      return account;
    }

    const { username, email, authenticated } = await this.accountApi.signIn(accountData);
    const account = new Account(username, email, authenticated);
    return account;
  }

  async signOut(account) {
    const authenticated = await this.accountApi.signOut(account);
    account.setAuthentication(authenticated);
    return account;
  }

  async isAuthenticated(account) {
    const authenticated = await this.accountApi.isAuthenticated(account);
    account.setAuthentication(authenticated);
    return account;
  }
}

export default AccountService;
