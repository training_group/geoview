

class Account {
  constructor(username, email, authenticated, rootProjectId) {
    this.username = 'Test User';
    this.email = email;
    this.authenticated = authenticated;
    this.role = 'admin';
    this.rootProjectId = rootProjectId;
  }

  setAuthentication(authenticated) {
    this.isAuthenticated = authenticated;
  }

  isAuthenticated() {
    return this.isAuthenticated;
  }
}

export default Account;
