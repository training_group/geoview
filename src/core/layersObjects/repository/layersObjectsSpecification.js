class LayersObjectsSpecification {
  constructor({
    layers,
    geometry,
    spatialRel,
    distance,
}) {
    this.layers = layers;
    this.geometry = geometry;
    this.spatialRel = spatialRel;
    this.distance = distance;
  }
}

export default LayersObjectsSpecification;
