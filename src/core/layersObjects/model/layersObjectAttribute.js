import LayerField from '../../layers/model/layerField';

class LayersObjectAttribute extends LayerField {
  constructor(layerField, value) {
    super(layerField);
    this.value = value;
  }
}

export default LayersObjectAttribute;
