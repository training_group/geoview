class LayersObject {
  constructor({
    id,
    layerId,
    title,
    attributes,
    geometry,
  }) {
    this.id = id;
    this.layerId = layerId;
    this.title = title;
    this.attributes = attributes;
    this.geometry = geometry;
    this.files = [];
    this.links = [];
    this.pictures = [];
  }
}


export default LayersObject;
