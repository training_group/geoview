

class LayersObjectGeometry {
  constructor({ layerGeometryType, coordinates }) {
    this.layerGeometryType = layerGeometryType;
    this.coordinates = coordinates;
  }
}


export default LayersObjectGeometry;
