import spatialRelations from "../../spatialRelations/enum/spatialRelations";
import LayersObjectsSpecification from "../repository/layersObjectsSpecification";

class LayersObjectsService {
  constructor(repository) {
    this.repository = repository;
  }

  async intersectsBy(geometry, layers, distance) {
    const specData = {
      layers,
      geometry,
      spatialRel: spatialRelations.intersects,
      distance
    };
    const layersObjectsSpec = new LayersObjectsSpecification(specData);
    const layersObjects = await this.repository.findBy(layersObjectsSpec);
    return layersObjects;
  }

  async getLayerObjects(features, layers) {
    return await this.repository.getLayerObjects({ features, layers });
  }
}

export default LayersObjectsService;
