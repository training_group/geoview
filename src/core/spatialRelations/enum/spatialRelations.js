import SpatialRelation from '../spatialRelation';

const spatialRelations = {
  intersects: new SpatialRelation({ id: 'intersects', name: 'Пересечение' }),
  crosses: new SpatialRelation({ id: 'crosses', name: 'Полное пересечение' }),
  contains: new SpatialRelation({ id: 'contains', name: 'Содержит' }),
};


export default spatialRelations;
