export default class Layer {
  static create({
    id,
    title,
    visible,
    editable,
    fields,
    geometryType,
    displayField,
    data
  }) {
    const layer = new Layer(id);
    layer.id = id;
    layer.visible = visible;
    layer.editable = editable;
    layer.title = title;
    layer.fields = fields;
    layer.geometryType = geometryType;
    layer.displayField = displayField;
    layer.data = data;
    return layer;
  }

  constructor(id) {
    this.id = id;
    this.title = `Новый слой - ${id}`;
    this.visible = true;
  }

  setVisible(visible) {
    this.visible = visible;
  }
}
