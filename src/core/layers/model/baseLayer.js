class BaseLayer {
  constructor({
    id,
    title,
    previewUrl,
    data,
  }) {
    this.id = id;
    this.title = title;
    this.previewUrl = previewUrl;
    this.data = data;
  }
}

export default BaseLayer;
