class LayerField {
  constructor({
    name,
    alias,
    type,
    visible,
    isEditable,
    required,
    inputType,
  }) {
    this.name = name;
    this.alias = alias;
    this.isEditable = isEditable;
    this.type = type;
    this.visible = visible;
    this.required = required;
    this.inputType = inputType;
  }
}


export default LayerField;
