const layerGeometryTypes = {
  point: 'point',
  multiPoint: 'multiPoint',
  line: 'line',
  polygon: 'polygon',
};

export default layerGeometryTypes;
