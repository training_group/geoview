const layerFieldTypes = {
  string: 'string',
  number: 'number',
  boolean: 'boolean',
  date: 'date',
};

export default layerFieldTypes;
