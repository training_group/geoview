class LayersService {
  static changeVisibility(currentLayer, visible, layers) {
    if (!currentLayer) {
      return layers;
    }

    const layersToUpdate = { ...layers };

    LayersService.onChangeLayerVisibility(currentLayer, visible);

    return layersToUpdate;
  }

  static setLayerVisibility(layer, visible) {
    const currentLayerItem = layer;
    currentLayerItem.visible = visible;
    return currentLayerItem;
  }

  static onChangeLayerVisibility(currentLayer, visible) {
    LayersService.setLayerVisibility(currentLayer, visible);
  }

  constructor(source) {
    this.source = source;
  }

  async getLayers(user) {
    const { baseLayers, layers } = await this.source.getLayers(user);

    return { baseLayers, layers };
  }

  async updateLayer({ id, features }) {
    return this.source.updateLayer({ id, features });
  }

  async removeFeatures({ id, features }) {
    return this.source.removeFeatures({ id, features });
  }
}

export default LayersService;
